'use strict';

var AWS = require('aws-sdk');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

AWS.config.update({ region: process.env['REGION'] });

var dynamoDB = new AWS.DynamoDB.DocumentClient();

var isEmpty = function(obj) {
return Object.keys(obj).length === 0;
};

function error_response(callback) {
    let internalresponsebody = {
        "code": STATUSCODE.ERRORFETCHINGDATAFROMDB,
        "message": "Failed to Fetch Data. Please try Again",
        "data": []
    };
    let errorresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    callback(null, errorresponse);
}


function success_response(callback, response) {
    let internalresponsebody = {
        "code": STATUSCODE.SUCCESS,
        "message": "SuccessFully Fetched all the data",
        "data": [response]
    };
    let successresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    console.log("response: " + JSON.stringify(successresponse));
    callback(null, successresponse);
}

async function get_Transactions(tablename, uuid, start, end) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        KeyConditionExpression: "#uuid = :uuid and transactionTimestamp between :start and :end",
        ExpressionAttributeNames:{
            "#uuid": "uuid"
        },
        ExpressionAttributeValues: {
            ":uuid": uuid,
            ":start": start,
            ":end": end
        }
    };
    try {
        let data = await dynamoDB.query(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}


exports.handler =  async function(event, context, callback) {
    const usersTransactionsTable = process.env['USER_TRANSACTIONS_TABLE'];
    
    var userinput = JSON.parse(event.body);
        
    var userTransactions = await get_Transactions(usersTransactionsTable, userinput.uuid, userinput.startTime, userinput.endTime);
    if(isEmpty(userTransactions)){
        error_response(callback);
    }
    
    var response = userTransactions.Items;
    
    success_response(callback, response);
}