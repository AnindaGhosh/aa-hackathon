'use strict';

var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
var AWS = require('aws-sdk');
const fs = require('fs');

AWS.config.update({region: process.env['REGION']});
exports.handler =  function(event, context, callback) {
    var poolData = {
        "UserPoolId": process.env['COGNITO_USER_POOL_ID'],
        "ClientId": process.env['COGNITO_APP_CLIENT_ID']
    };
    
    var userDetails = JSON.parse(event.body);
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: userDetails.email,
        Pool: userPool,
    };
    
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    cognitoUser.resendConfirmationCode(function(err, result) {
        if (err) {
            let internalresponsebody = {
                "message" : err.message,
                "data" : []
            };
            if(err.code === "InvalidParameterException")
                internalresponsebody.code = STATUSCODE.INVALIDPARAMETEREXCEPTION;
            else
                internalresponsebody.code = STATUSCODE.UNKNOWNEXCEPTION;
            
                let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(errorresponse));
            callback(null, errorresponse);
        }
        let internalresponsebody = {
            "code" : 200,
            "message" : "Successfully re-sent Confirmation OTP",
            "data" : []
        };
        let successresponse = {
            "statusCode": 200,
            "body": JSON.stringify(internalresponsebody)
        };
        console.log("response: " + JSON.stringify(successresponse));
        callback(null, successresponse);
    });
}