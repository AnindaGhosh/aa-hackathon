'use strict';

const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

exports.handler = (event, context, callback) => {
    const refreshTokenFromUser = event.headers['RefreshToken'];
    const RefreshToken = new AmazonCognitoIdentity.CognitoRefreshToken({RefreshToken: refreshTokenFromUser});

    // Define AWS Cognito User Pool
    var poolData = {
        "UserPoolId": process.env['COGNITO_USER_POOL_ID'],
        "ClientId": process.env['COGNITO_APP_CLIENT_ID']
    };

    var userDetails = JSON.parse(event.body);

    const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    const userData = {
        Username: userDetails.email,
        Pool: userPool
    };

    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    cognitoUser.refreshSession(RefreshToken, (err, session) => {
        if (err) {
            let internalresponsebody = {
                "message" : err.message,
                "data" : []
            };
            if(err.code === "NotAuthorizedException")
                internalresponsebody.code = STATUSCODE.NOTAUTHORIZEDEXCEPTION;
            else
                internalresponsebody.code = STATUSCODE.UNKNOWNEXCEPTION;
            let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(errorresponse));
            callback(null, errorresponse);
        } else {
            let internalresponsebody = {
                "code" : STATUSCODE.SUCCESS,
                "message" : "Access Token Refreshed",
                "data" : [{
                    "accessToken": session.accessToken.jwtToken,
                    "idToken": session.idToken.jwtToken,
                    "refreshToken": session.refreshToken.token,
                }]
            };
            let successresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(successresponse));
            callback(null, successresponse);
        }
    });
}