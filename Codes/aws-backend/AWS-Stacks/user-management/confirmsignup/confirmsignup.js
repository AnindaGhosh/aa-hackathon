'use strict';

var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
var AWS = require('aws-sdk');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

AWS.config.update({region: process.env['REGION']});

var dynamoDB = new AWS.DynamoDB.DocumentClient();


exports.handler =  function(event, context, callback) {
    var poolData = {
        "UserPoolId": process.env['COGNITO_USER_POOL_ID'],
        "ClientId": process.env['COGNITO_APP_CLIENT_ID']
    };
    
    var UsersTable = process.env['USER_DETAILS_TABLE'];

    var userDetails = JSON.parse(event.body);
    var passcode = userDetails.passcode;
    console.log(userDetails.email);

    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: userDetails.email,
        Pool: userPool,
    };
    
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.confirmRegistration(passcode, true, function(err, result) {
        if (err) {
            let internalresponsebody = {
                "message" : err.message,
                "data" : []
            };
            if(err.code === "ExpiredCodeException")
                internalresponsebody.code = STATUSCODE.EXPIREDCODEEXCEPTION;
            else if(err.code === "CodeMismatchException")
                internalresponsebody.code = STATUSCODE.CODEMISMATCHEXCEPTION;
            else
                internalresponsebody.code = STATUSCODE.UNKNOWNEXCEPTION;
            let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(errorresponse));
            callback(null, errorresponse);
        }else{
            var params = {
                TableName: UsersTable,
                Key: {
                    'email': userDetails.email
                }
            };

            // Call DynamoDB to read the item from the table
            dynamoDB.get(params, function(err, data) {
                if (err) {
                    console.log("Error", err);
                } else {
                    console.log("Success");
                    let responsedata = data;
                    let updated_at = Math.floor(new Date() / 1000);
                    responsedata.Item.updatedAt = updated_at.toString();
                    responsedata.Item.status = "ACTIVE";
                    responsedata.Item.onboardStatus = false;

                    var params = {
                        TableName: UsersTable,
                        Item: responsedata.Item
                    };
                    // Call DynamoDB to add the item to the table
                    dynamoDB.put(params, function(err, data) {
                        if (err) {
                            // Ignore for the moment
                            console.log("Error", err);
                        } else {
                            console.log("Success", data);
                        }
                    });
                }
            });


            let internalresponsebody = {
                "code" : STATUSCODE.SUCCESS,
                "message" : "Congratuations! Your signup is confirmed",
                "data" : []
            };
            let successresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(successresponse));
            callback(null, successresponse);
        }
    });  
};