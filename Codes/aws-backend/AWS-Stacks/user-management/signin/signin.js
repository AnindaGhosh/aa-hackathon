'use strict';
var AWS = require('aws-sdk');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
global.fetch = require('node-fetch');
const fs = require('fs');

var dynamoDB = new AWS.DynamoDB.DocumentClient();

var isEmpty = function(obj) {
return Object.keys(obj).length === 0;
};

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

function error_response(callback) {
    let internalresponsebody = {
        "code": STATUSCODE.ERRORFETCHINGDATAFROMDB,
        "message": "Failed to Fetch Data. Please try Again",
        "data": []
    };
    let errorresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    callback(null, errorresponse);
}

async function get_userDetails(tablename, userdetails) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'email': userdetails.email
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}

exports.handler = function(event, context, callback) {
  
    var usersDetailsTable = process.env['USER_DETAILS_TABLE'];

    var userDetails = JSON.parse(event.body);

    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username: userDetails.email,
        Password: userDetails.password,
    });

    // Define AWS Cognito User Pool
    var poolData = {
        "UserPoolId": process.env['COGNITO_USER_POOL_ID'],
        "ClientId": process.env['COGNITO_APP_CLIENT_ID']
    };

    var userPool = new CognitoUserPool(poolData);

    var userData = {
        Username: userDetails.email,
        Pool: userPool
    };

    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    console.log("Getting Access Token")
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: async function (result) {           
            var onboard_status;
            console.log("Fetching Onboard Status")
            var data = await get_userDetails(usersDetailsTable, userDetails);
            if(isEmpty(data)){
                error_response(callback);
            }else{
                onboard_status = data.Item.onboardStatus;
            }
            let internalresponsebody = {
                "code" : STATUSCODE.SUCCESS,
                "message" : "Access Tokens",
                "data" : [{
                    "accessToken": result.getAccessToken().getJwtToken(),
                    "tokenId": result.getIdToken().getJwtToken(),
                    "refreshToken": result.getRefreshToken().getToken(),
                    "onboardStatus": onboard_status
                }]
            };
            let successresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(successresponse));
            callback(null, successresponse);
        },
        onFailure: async function (err) {
            console.log(err);
            let internalresponsebody = {
                "message" : err.message,
                "data" : []
            };
            if(err.code === "NotAuthorizedException")
                internalresponsebody.code = STATUSCODE.NOTAUTHORIZEDEXCEPTION;
            else
                internalresponsebody.code = STATUSCODE.UNKNOWNEXCEPTION;
            let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            console.log("response: " + JSON.stringify(errorresponse));
            callback(null, errorresponse);
        },
    });
}