'use strict';

var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
var CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
var CognitoUserAttribute = AmazonCognitoIdentity.CognitoUserAttribute;
const { v5: uuidv5 } = require('uuid');
global.fetch = require('node-fetch');
var AWS = require('aws-sdk');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

AWS.config.update({region: process.env['REGION']});

var dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler =  function(event, context, callback) {
    // Define AWS Cognito User Pool
    let poolData = {
        "UserPoolId": process.env['COGNITO_USER_POOL_ID'],
        "ClientId": process.env['COGNITO_APP_CLIENT_ID']
    };
    
    var UsersTable = process.env['USER_DETAILS_TABLE'];

    let userPool = new CognitoUserPool(poolData);
    // console.log('userPool:', userPool);
    
    let updated_at = Math.floor(new Date() / 1000);
    let userDetails = JSON.parse(event.body);
    console.log(userDetails.email);
    
    // Define User Attributes
    let attributeList = [];
    let attributeEmail = new CognitoUserAttribute({"Name":"email","Value":userDetails.email});
    let attributePhNumber = new CognitoUserAttribute({"Name":"phone_number","Value":userDetails.phone_number});
    let attributeName = new CognitoUserAttribute({"Name":"name","Value":userDetails.name});
    let attributeupdatedAt = new CognitoUserAttribute({"Name":"updated_at","Value":updated_at.toString()});

    attributeList.push(attributeEmail);
    attributeList.push(attributePhNumber);
    attributeList.push(attributeName);
    attributeList.push(attributeupdatedAt);

    // Create User via AWS Cognito
    userPool.signUp(userDetails.email, userDetails.password, attributeList, null, function (err, result) {
        if (err) {
            let internalresponsebody = {
                "message" : err.message,
                "data" : []
            };
            if(err.code === "InvalidPasswordException")
                internalresponsebody.code = STATUSCODE.INVALIDPASSWORDEXCEPTION;
            else if(err.code === "InvalidParameterException")
                internalresponsebody.code = STATUSCODE.INVALIDPARAMETEREXCEPTION;
            else
                internalresponsebody.code = STATUSCODE.UNKNOWNEXCEPTION;
            let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            // console.log("response: " + JSON.stringify(errorresponse));
            callback(null, errorresponse);
        }else{
            var uuid = uuidv5(userDetails.email, process.env['UUID_SECRET']);
            // Prepare the DynamoDB insertion object insertion
            let ddbobj = {
                "email" : userDetails.email,
                "phnumber" : userDetails.phone_number,
                "uuid" : uuid,
                "createdAt" : updated_at.toString(),
                "updatedAt" : updated_at.toString(),
                "status" : "INACTIVE",
                "onboardStatus" : false
            }
            
            var params = {
                TableName: UsersTable,
                Item: ddbobj
            };
              
              // Call DynamoDB to add the item to the table
            dynamoDB.put(params, function(err, data) {
                if (err) {
                    console.log("Error", err);
                } else {
                    console.log("Success", data);
                }
            });

            let internalresponsebody = {
                "code" : STATUSCODE.SUCCESS,
                "message" : "Successfully Registered. Please confirm your OTP",
                "data" : [{
                    "uuid": uuid
                }]
            };
            let successresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            // console.log("response: " + JSON.stringify(successresponse));
            callback(null, successresponse);   
        }
    });  
};