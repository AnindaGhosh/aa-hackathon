const axios = require('axios');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

const client_id = process.env['CLIENT_ID'];
const client_secret = process.env['CLIENT_SECRET'];
const org_id = process.env['ORGANISATION_ID'];

exports.handler = (event, context, callback) => {
    var consentRequestDetails = JSON.parse(event.body);

    var phonenumber = consentRequestDetails.phonenumber;
    var aggregator = consentRequestDetails.aggregator;
    var consent_types = consentRequestDetails.consent_types;
    var aggegator_id = phonenumber.concat('@').concat(aggregator);

    var no_of_consents = consent_types.length;
    var axios_instances = [];

    for(let i=0; i<no_of_consents; i++){
        axios_instances.push(axios({
            method: 'post',
            url: 'https://sandbox.moneyone.in/finpro_sandbox/v2/requestconsent',
            headers: { 
            'Content-Type': 'application/json', 
            'client_id': client_id, 
            'client_secret': client_secret, 
            'organisationId': org_id
            },
            data : JSON.stringify({"partyIdentifierType":"MOBILE","partyIdentifierValue":phonenumber,"productID":consent_types[i],"accountID":phonenumber,"vua":aggegator_id})
        }));
    }
    axios.all(axios_instances)
    .then(function(results) {
        let temp = results.map(r => r.data);
        var data = [];
        for(let i=0; i<temp.length; i++){
            data.push({
                "status" : temp[i].data.status,
                "consent_handle" : temp[i].data.consent_handle
            });
        }
        let internalresponsebody = {
            "code" : STATUSCODE.SUCCESS,
            "message" : "Successfully created consents",
            "data" : data
        };
        let successresponse = {
            "statusCode": 200,
            "body": JSON.stringify(internalresponsebody)
        };
        console.log("response: " + JSON.stringify(successresponse));
        callback(null, successresponse);
    }).catch(error => {
        let internalresponsebody = {
            "code" : STATUSCODE.BADREQUEST,
            "message" : error.response.data,
            "data" : []
        };
        if(error.response.data.errorCode === "InvalidProductID")
            internalresponsebody.code = STATUSCODE.INVALIDPRODUCTID;
        else if(error.response.data.errorCode === "InvalidCustomerAddress")
            internalresponsebody.code = STATUSCODE.INVALIDCUSTOMERADDRESS;
        let errorresponse = {
            "statusCode": 200,
            "body": JSON.stringify(internalresponsebody)
        };
        callback(null, errorresponse);
    });
}