const axios = require('axios');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

const client_id = process.env['CLIENT_ID'];
const client_secret = process.env['CLIENT_SECRET'];
const org_id = process.env['ORGANISATION_ID'];

exports.handler = (event, context, callback) => {
    var allFiDataRequest = JSON.parse(event.body);
    var consentId = allFiDataRequest.consentId;
    var consentType = allFiDataRequest.consentType;
    var axiosInstance = axios({
        method: 'post',
        url: 'https://sandbox.moneyone.in/finpro_sandbox/getallfidata',
        headers: { 
            'Content-Type': 'application/json', 
            'client_id': client_id, 
            'client_secret': client_secret, 
            'organisationId': org_id
        },
        data : JSON.stringify({"consentID": consentId})
    });

    axiosInstance
    .then(function(response) {
            let tempData = response.data.data;
            data = [];
            for(var i=0; i<tempData.length; i++){
                data.push({
                    "linkReferenceNumber" : tempData[i].linkReferenceNumber,
                    "maskedAccountNumber": tempData[i].maskedAccountNumber,
                    "bankName": tempData[i].bank,
                    "summary" : tempData[i].Summary,
                    "profile" : {
                        "type": tempData[i].Profile.Holders.type,
                        "holderInformation": tempData[i].Profile.Holders.Holder[0],
                    }
                })
            }
            let internalresponsebody = {
                "code" : STATUSCODE.SUCCESS,
                "message" : "Successfully fetch the account profile and summary from all the linked FIs associated with the consent",
                "data" : data
            };
            let successresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            //console.log("response: " + JSON.stringify(successresponse));
            callback(null, successresponse);
    }).catch(error => {
            let internalresponsebody = {
                "code" : STATUSCODE.BADREQUEST,
                "message" : error.response.data,
                "data" : []
            };
            let errorresponse = {
                "statusCode": 200,
                "body": JSON.stringify(internalresponsebody)
            };
            callback(null, errorresponse);
    });
}