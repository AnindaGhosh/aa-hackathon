const axios = require('axios');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

const client_id = process.env['CLIENT_ID'];
const client_secret = process.env['CLIENT_SECRET'];
const org_id = process.env['ORGANISATION_ID'];

exports.handler = (event, context, callback) => {
    var fiDataRequest = JSON.parse(event.body);
    const consentId = fiDataRequest.consentId;
    const linkReferenceNumber = fiDataRequest.linkReferenceNo

    var axiosInstance = axios({
        method: 'post',
        url: 'https://sandbox.moneyone.in/finpro_sandbox/getfidata',
        headers: { 
            'Content-Type': 'application/json', 
            'client_id': client_id, 
            'client_secret': client_secret, 
            'organisationId': org_id
        },
        data : JSON.stringify({"consentID": consentId,"linkReferenceNumber":linkReferenceNumber})
    });
    
    axiosInstance
    .then(function(response) {
        let tempData = response.data;
        let data = [];
        data.push({
            "linkReferenceNumber" : tempData.data.linkReferenceNumber,
            "maskedAccountNumber": tempData.data.maskedAccountNumber,
            "balance": tempData.data.balance,
            "fipName" : tempData.data.fipName,
            "fipId": tempData.data.fipId,
            "fiType" : tempData.data.fiType,
            "fiData" : tempData.data.fiData
        });

        let internalresponsebody = {
            "code" : STATUSCODE.SUCCESS,
            "message" : "Successfully fetch the FI Data",
            "data" : data
        };
        let successresponse = {
            "statusCode": 200,
            "body": JSON.stringify(internalresponsebody)
        };
        //console.log("response: " + JSON.stringify(successresponse));
        callback(null, successresponse);
    }).catch(error => {
        let internalresponsebody = {
            "code" : STATUSCODE.BADREQUEST,
            "message" : "Bad Request",
            "data" : []
        };
        let errorresponse = {
            "statusCode": 200,
            "body": JSON.stringify(internalresponsebody)
        };
        callback(null, errorresponse);
    });
}