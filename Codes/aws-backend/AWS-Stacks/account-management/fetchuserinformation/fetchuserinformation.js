'use strict';

var AWS = require('aws-sdk');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

AWS.config.update({ region: process.env['REGION'] });

var dynamoDB = new AWS.DynamoDB.DocumentClient();

var isEmpty = function(obj) {
return Object.keys(obj).length === 0;
};

function error_response(callback) {
    let internalresponsebody = {
        "code": STATUSCODE.ERRORFETCHINGDATAFROMDB,
        "message": "Failed to Fetch Data. Please try Again",
        "data": []
    };
    let errorresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    callback(null, errorresponse);
}


function success_response(callback, response) {
    let internalresponsebody = {
        "code": STATUSCODE.SUCCESS,
        "message": "SuccessFully Saved all the details",
        "data": [response]
    };
    let successresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    console.log("response: " + JSON.stringify(successresponse));
    callback(null, successresponse);
}

async function get_userDetails(tablename, userdetails) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'email': userdetails.email
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}

async function get_aaDetails(tablename, uuid) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'uuid': uuid
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}


async function get_financialservicesDetails(tablename, uuid) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'uuid': uuid
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}

async function get_consentDetails(tablename, uuid) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'uuid': uuid
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}


exports.handler =  async function(event, context, callback) {
    const usersDetailsTable = process.env['USER_DETAILS_TABLE'];
    const userFinancialServicesTable = process.env['FINANCIAL_DETAILS_TABLE'];
    const userAADetailsTable = process.env['AA_DETAILS_TABLE'];
    const userConsentDetailsTable = process.env['CONSENT_DETAILS_TABLE'];
    
    var userinput = JSON.parse(event.body);
        
    var uuid;
    
    var userDetails = await get_userDetails(usersDetailsTable, userinput);
    if(isEmpty(userDetails)){
        error_response(callback);
    }else{
        uuid = userDetails.Item.uuid;
    }
    
    var userAADetails = await get_aaDetails(userAADetailsTable, uuid);
    if(isEmpty(userDetails)){
        error_response(callback);
    }
    
    var userFinancialServices = await get_financialservicesDetails(userFinancialServicesTable, uuid);
    if(isEmpty(userFinancialServices)){
        error_response(callback);
    }
    
    var userConsentDetails = await get_consentDetails(userConsentDetailsTable, uuid);
    if(isEmpty(userConsentDetails)){
        error_response(callback);
    }
    
    var response = {};
    response.userDetails = userDetails.Item;
    response.aaDetails = userAADetails.Item.aa;
    response.financialServices = userFinancialServices.Item.financialServices;
    response.consentDetails = userConsentDetails.Item.consents;
    
    success_response(callback, response);
}