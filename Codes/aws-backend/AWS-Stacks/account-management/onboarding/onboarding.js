'use strict';

var AWS = require('aws-sdk');
const fs = require('fs');

var statusCodeConfig = fs.readFileSync('statuscode_config.json');
var STATUSCODE = JSON.parse(statusCodeConfig);

AWS.config.update({ region: process.env['REGION'] });

var dynamoDB = new AWS.DynamoDB.DocumentClient();

var isEmpty = function(obj) {
  return Object.keys(obj).length === 0;
};

function error_response(callback) {
    let internalresponsebody = {
        "code": STATUSCODE.ERRORSAVINGDATATODB,
        "message": "Failed to Save Data. Please try Again",
        "data": []
    };
    let errorresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    // console.log("response: " + JSON.stringify(errorresponse));
    callback(null, errorresponse);
}


function success_response(callback) {
    let internalresponsebody = {
        "code": STATUSCODE.SUCCESS,
        "message": "SuccessFully Saved all the details",
        "data": []
    };
    let successresponse = {
        "statusCode": 200,
        "body": JSON.stringify(internalresponsebody)
    };
    console.log("response: " + JSON.stringify(successresponse));
    callback(null, successresponse);
}


async function get_userDetails(tablename, userdetails) {
    // Read and Update in the UserDetailsTable
    let params = {
        TableName: tablename,
        Key: {
            'email': userdetails.email
        }
    };
    try {
        let data = await dynamoDB.get(params).promise();
        return data;
    } catch(err) {
        console.log("call error");
        console.log(err);
    }
}

async function update_userDetails(tablename, userdetails, inputdata, timestamp) {
    let responsedata = inputdata;

    responsedata.Item.firstName = userdetails.firstName;
    responsedata.Item.lastName = userdetails.lastName;
    responsedata.Item.gender = userdetails.gender;
    responsedata.Item.dob = userdetails.dob;
    responsedata.Item.incomeRange = userdetails.incomeRange;
    responsedata.Item.pan = userdetails.pan;
    responsedata.Item.updatedAt = timestamp.toString();
    responsedata.Item.onboardStatus = true;

    let params = {
        TableName: tablename,
        Item: responsedata.Item
    };
    // Call DynamoDB to add the item to the table
    
    try {
        await dynamoDB.put(params).promise();
        return 1;
    } catch(err) {
        console.log("call error");
        console.log(err);
        return 0;
    }
}


async function insert_aaDetails(tablename, aadetails, uuid, timestamp) {
    let ddbobj = {
        "uuid": uuid,
        "aa": aadetails,
        "timeStamp": timestamp.toString()
    };

    let params = {
        TableName: tablename,
        Item: ddbobj
    };

    try {
        await dynamoDB.put(params).promise();
        return 1;
    } catch(err) {
        console.log(err);
        return 0;
    }
}

async function insert_financialservicesDetails(tablename, fsdetails, uuid, timestamp) {
    let ddbobj = {
        "uuid": uuid,
        "financialServices": fsdetails,
        "timeStamp": timestamp.toString()
    };

    let params = {
        TableName: tablename,
        Item: ddbobj
    };

    try {
        await dynamoDB.put(params).promise();
        return 1;
    } catch(err) {
        console.log(err);
        return 0;
    }
}

async function insert_consentDetails(tablename, consentdetails, uuid, timestamp) {
    let ddbobj = {
        "uuid": uuid,
        "consents": consentdetails,
        "timeStamp": timestamp.toString()
    };

    let params = {
        TableName: tablename,
        Item: ddbobj
    };

    try {
        await dynamoDB.put(params).promise();
        return 1;
    } catch(err) {
        console.log(err);
        return 0;
    }
}

exports.handler = async function (event, context, callback) {
    var userDetailsTable = process.env['USER_DETAILS_TABLE'];
    var userFinancialServicesTable = process.env['FINANCIAL_DETAILS_TABLE'];
    var userAADetailsTable = process.env['AA_DETAILS_TABLE'];
    var userConsentDetailsTable = process.env['CONSENT_DETAILS_TABLE'];

    var profileBuilder = JSON.parse(event.body);
    console.log(profileBuilder);

    var uuid = profileBuilder.uuid;
    var userDetails = profileBuilder.userDetails;
    var userFinancialServices = profileBuilder.financialServices;
    var userAADetails = profileBuilder.aaDetails;
    var userConsentDetails = profileBuilder.consentDetails;

    var currTimeStamp = Math.floor(new Date() / 1000);
    
    var storeddata = await get_userDetails(userDetailsTable,userDetails);
    if(isEmpty(storeddata)){
        error_response(callback);
    }
    else{
        let response = await update_userDetails(userDetailsTable, userDetails, storeddata, currTimeStamp);
        if(response){
            let response = await insert_aaDetails(userAADetailsTable, userAADetails, uuid, currTimeStamp);
            if(response){
                let response = await insert_financialservicesDetails(userFinancialServicesTable, userFinancialServices, uuid, currTimeStamp);
                if(response){
                    let response = await insert_consentDetails(userConsentDetailsTable, userConsentDetails, uuid, currTimeStamp);
                    if(response){
                        success_response(callback);    
                    }else{
                       error_response(callback);
                    }
                }else{
                   error_response(callback);
                }    
            }else{
                error_response(callback);
            }
        }else{
            error_response(callback);
        }
    }
};