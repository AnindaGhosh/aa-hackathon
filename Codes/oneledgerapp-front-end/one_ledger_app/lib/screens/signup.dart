import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/registerModel.dart';
import 'package:one_ledger_app/utils/form_submissions/signUpFormSubmit.dart';
import 'package:one_ledger_app/widgets/forms/signup_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/widgets/linkText.dart';
import 'package:one_ledger_app/widgets/loader.dart';
import 'package:one_ledger_app/widgets/logo.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _signupformKey = GlobalKey<FormState>();
  SignUpModal _signUpData;
  ValueNotifier<bool> loading = new ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: linkPlusNormalText(
                  linkTextContent: "Login Here",
                  normalTextContent: "Already a member?",
                  onTap: () {
                    Navigator.pushNamed(context, '/login');
                  },
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        logo(),
                        SizedBox(
                          height: 30,
                        ),
                        heading(
                          title: "Sign Up",
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        signUpForm(
                          context: context,
                          key: _signupformKey,
                          onFormSubmit: (savedData) async {
                            if (loading.value == false) {
                              _signupformKey.currentState.save();
                              if (_signupformKey.currentState.validate()) {
                                loading.value = true;
                                _signUpData = savedData;
                                await signUpCheck(
                                    context: context, signUpData: _signUpData);
                                loading.value = false;
                              }
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  ValueListenableBuilder(
                    valueListenable: loading,
                    builder:
                        (BuildContext context, dynamic value, Widget child) {
                      return value ? customLoader() : SizedBox();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
