import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/loginModel.dart';
import 'package:one_ledger_app/utils/form_submissions/loginFormSubmit.dart';
import 'package:one_ledger_app/widgets/forms/login_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/widgets/loader.dart';
import 'package:one_ledger_app/widgets/logo.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var _loginformKey = GlobalKey<FormState>();
  LoginModal _loginData;
  ValueNotifier<bool> loading = new ValueNotifier(false);

  @override
  void initState() {
    super.initState();
    _loginformKey = new GlobalKey<FormState>();
    _loginData = new LoginModal();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  logo(),
                  SizedBox(
                    height: 30,
                  ),
                  heading(
                    title: "Welcome to One Ledger Book",
                  ),
                  SizedBox(height: 30),
                  loginForm(
                    context: context,
                    key: _loginformKey,
                    onFormSubmit: (savedData) async {
                      if (loading.value == false) {
                        _loginformKey.currentState.save();
                        if (_loginformKey.currentState.validate()) {
                          loading.value = true;
                          _loginData = savedData;
                          await loginCheck(
                              context: context, loginData: _loginData);
                          loading.value = false;
                        }
                      }
                    },
                  ),
                ],
              ),
            ),
            ValueListenableBuilder(
              valueListenable: loading,
              builder: (BuildContext context, dynamic value, Widget child) {
                return value ? customLoader() : SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
