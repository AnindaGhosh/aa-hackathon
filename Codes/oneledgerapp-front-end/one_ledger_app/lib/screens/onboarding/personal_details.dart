import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/personalDetailsModel.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/utils/form_submissions/personalDetailsFormSubmit.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/forms/onboarding_forms/personal_details_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

class PersonalDetails extends StatefulWidget {
  PersonalDetails({Key key}) : super(key: key);

  @override
  _PersonalDetailsState createState() => _PersonalDetailsState();
}

class _PersonalDetailsState extends State<PersonalDetails> {
  PersonalDetailsModel _personalDetailsData;
  UserDetails userDetail;
  @override
  void initState() {
    super.initState();
    _personalDetailsData = new PersonalDetailsModel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          // alignment: Alignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    backImageButton(context: context),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingHeading(title: "Personal Details"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingSubHeading(
                          subHeading: "Enter your personal details here."),
                    ),
                    SizedBox(height: 30),
                    PersonalDetailsForm(
                      onFormSubmit: (savedData) async {
                        _personalDetailsData = savedData;
                        createUseDetails(_personalDetailsData);
                        userDetail = fetchUseDetails();
                        savePersonalDetails(
                            context: context, personalDetailData: userDetail);
                        //Navigator.pushNamed(context, '/financialServices');
                      },
                    ),
                  ],
                ),
              ),
            ),
            // ValueListenableBuilder(
            //   valueListenable: loading,
            //   builder: (BuildContext context, dynamic value, Widget child) {
            //     return value ? customLoader() : SizedBox();
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
