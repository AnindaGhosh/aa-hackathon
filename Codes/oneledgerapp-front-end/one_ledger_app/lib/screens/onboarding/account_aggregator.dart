import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/accountAggregatorModel.dart';
import 'package:one_ledger_app/utils/form_submissions/accountAggregratorFormSubmit.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/forms/onboarding_forms/account_aggregator_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';

class AccountAggregator extends StatefulWidget {
  @override
  _AccountAggregatorState createState() => _AccountAggregatorState();
}

class _AccountAggregatorState extends State<AccountAggregator> {
  AccountAggregatorModel _accountAggregatorData;

  @override
  void initState() {
    super.initState();
    _accountAggregatorData = new AccountAggregatorModel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          // alignment: Alignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    backImageButton(context: context),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingHeading(title: "Account Aggregator IDs"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingSubHeading(
                          subHeading:
                              "Enter the account aggregator ID (Maximum 5)."),
                    ),
                    SizedBox(height: 30),
                    AccountAggregatorForm(
                      onFormSubmit: (savedData) async {
                        _accountAggregatorData = savedData;
                        await saveAADetails(
                            context: context, aaModel: _accountAggregatorData);
                        //Navigator.pushNamed(context, '/consentTemplate');
                      },
                    ),
                  ],
                ),
              ),
            ),
            // ValueListenableBuilder(
            //   valueListenable: loading,
            //   builder: (BuildContext context, dynamic value, Widget child) {
            //     return value ? customLoader() : SizedBox();
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
