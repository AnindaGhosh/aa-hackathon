import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/utils/form_submissions/autoOnboardingProcess.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/forms/onboarding_forms/consent_template_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

class ConsentTemplate extends StatefulWidget {
  @override
  _ConsentTemplateState createState() => _ConsentTemplateState();
}

class _ConsentTemplateState extends State<ConsentTemplate> {
  final _consentTemplateFormKey = GlobalKey<FormState>();
  ConsentTemplateModel _consentTemplateData;
  Map<String, String> userBasicInfo = getUserBasicInformation();
  ValueNotifier<bool> loading = new ValueNotifier(false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          // alignment: Alignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    backImageButton(context: context),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingHeading(title: "Consent Details"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingSubHeading(
                          subHeading:
                              "Please select the required permission for fetching your information from respective Financial Providers"),
                    ),
                    SizedBox(height: 30),
                    consentTemplateForm(
                      context: context,
                      key: _consentTemplateFormKey,
                      onFormSubmit: (savedData) async {
                        _consentTemplateFormKey.currentState.save();
                        if (_consentTemplateFormKey.currentState.validate()) {
                          loading.value = true;
                          _consentTemplateData = savedData;
                          _consentTemplateData.email = userBasicInfo['email'];
                          _consentTemplateData.phoneNumber =
                              userBasicInfo['phoneNumber'];
                          _consentTemplateData.aggregator = aggregatorList[0];
                          //await createProfileFromDB(context: context);
                          await startAutoOnboarding(
                              context: context,
                              consentTemplateData: _consentTemplateData);
                          loading.value = false;
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            // ValueListenableBuilder(
            //   valueListenable: loading,
            //   builder: (BuildContext context, dynamic value, Widget child) {
            //     return value ? customLoader() : SizedBox();
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
