import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/colorCodes.dart';
import 'package:one_ledger_app/constants/textStyles.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';
import 'package:one_ledger_app/widgets/alerts.dart';

import 'package:one_ledger_app/widgets/logo.dart';

ValueNotifier<double> valueToLoader = new ValueNotifier(0.0);
ValueNotifier<String> textToLoader = new ValueNotifier(
    "Your Consent has been prepared successfully. Sending the request of your consent for approval to respective AA.");

//to chnage
class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  int percent = 0;

  @override
  void initState() {
    super.initState();

    //Timer.periodic(Duration(seconds: 2), (timer) {
    //  valueToLoader.value += 0.1;
    //});

    controller = AnimationController(vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        percent = (animation.value * 100).round();
        setState(() {});
      });

    // controller.forward();

    valueToLoader.addListener(() {
      // if condition
      if (checkForLoadedConsent() >= mandatoryConsents.length) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return showPopup(
                action1Text: "skip",
                action2Text: "continue",
                content: "Do you want to approve the others as well?",
                title: "Mandatory Consents Approved!",
                action2: () {
                  Navigator.pop(context);
                },
                action1: () {
                  setConsentFetchCompleted();
                  Navigator.pop(context);
                });
          },
        );
      }
      if (valueToLoader.value == 1) {
        Navigator.pushNamedAndRemoveUntil(
            context, '/landingPage', (route) => false);
      }
      controller.animateTo(
        valueToLoader.value,
        duration: Duration(
          milliseconds: 500,
        ),
      );
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.stop();
    super.dispose();
  }

  Text loadingText() {
    return Text(
      textToLoader.value,
      style: onBoardingSubHeadingTextStyle,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                logo(),
                Column(
                  children: <Widget>[
                    Text(
                      '$percent %',
                      style: headingTextStyle,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 45.0, vertical: 15.0),
                      child: LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        value: animation.value,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(splashScreenBG),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    loadingText(),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
