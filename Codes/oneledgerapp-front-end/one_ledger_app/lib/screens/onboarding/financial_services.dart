import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/financialServicesModel.dart';
import 'package:one_ledger_app/utils/form_submissions/financialServicesFormSubmit.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/forms/onboarding_forms/financial_services_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';

class FinancialServices extends StatefulWidget {
  @override
  _FinancialServicesState createState() => _FinancialServicesState();
}

class _FinancialServicesState extends State<FinancialServices> {
  FinancialServicesModel _financialServicesData;

  @override
  void initState() {
    super.initState();
    _financialServicesData = new FinancialServicesModel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          // alignment: Alignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    backImageButton(context: context),
                    SizedBox(
                      height: 30,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingHeading(title: "Financial Services"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: onBoardingSubHeading(
                          subHeading: "Select the below mentioned services."),
                    ),
                    SizedBox(height: 30),
                    FinancialServicesForm(
                      onFormSubmit: (savedData) async {
                        _financialServicesData = savedData;
                        await saveFinancialServicesDetails(
                            context: context,
                            financialService: _financialServicesData);
                      },
                    ),
                  ],
                ),
              ),
            ),
            // ValueListenableBuilder(
            //   valueListenable: loading,
            //   builder: (BuildContext context, dynamic value, Widget child) {
            //     return value ? customLoader() : SizedBox();
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
