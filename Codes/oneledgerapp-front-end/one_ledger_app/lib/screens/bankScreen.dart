import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/colorCodes.dart';
import 'package:one_ledger_app/constants/textStyles.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/widgets/sidemenu.dart';

class BankScreen extends StatefulWidget {
  @override
  _BankScreenState createState() => _BankScreenState();
}

class _BankScreenState extends State<BankScreen> {
  bool isExpanded = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      backgroundColor: Colors.grey[100],
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 60,
          ),
          backImageButton(context: context),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: onBoardingHeading(title: "Bank Details"),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: BarChart(
                BarChartData(
                  axisTitleData: FlAxisTitleData(
                    bottomTitle:
                        AxisTitle(showTitle: true, titleText: "FL Banks"),
                    topTitle: AxisTitle(
                        showTitle: true,
                        titleText: "Debit & Credit",
                        textStyle: headingTextStyle,
                        textAlign: TextAlign.left),
                  ),
                  borderData: FlBorderData(
                    show: false,
                  ),
                  alignment: BarChartAlignment.center,
                  groupsSpace: 40,
                  backgroundColor: Colors.white,
                  titlesData: FlTitlesData(
                    show: true,
                    bottomTitles: SideTitles(
                      showTitles: true,
                      textStyle: TextStyle(
                          color: const Color(0xff7589a2),
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      margin: 20,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return 'Axis Bank';
                          case 1:
                            return 'SBI';
                          default:
                            return '';
                        }
                      },
                    ),
                    leftTitles: SideTitles(
                      showTitles: true,
                      textStyle: normalTextStyle,
                      reservedSize: 20,
                      getTitles: (value) {
                        if (value == 0) {
                          return '0';
                        } else if (value == 500) {
                          return '500';
                        } else if (value == 1000) {
                          return '1000';
                        } else if (value == 1500) {
                          return '1500';
                        } else if (value == 2000) {
                          return '2000';
                        } else if (value == 2500) {
                          return '2500';
                        } else {
                          return '';
                        }
                      },
                    ),
                  ),
                  barGroups: [
                    BarChartGroupData(
                      x: 0,
                      barRods: [
                        BarChartRodData(y: 0, color: Colors.purple),
                        BarChartRodData(y: 500, color: splashScreenBG),
                      ],
                    ),
                    BarChartGroupData(
                      x: 1,
                      barRods: [
                        BarChartRodData(y: 1000, color: Colors.purple),
                        BarChartRodData(y: 2500, color: splashScreenBG),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
