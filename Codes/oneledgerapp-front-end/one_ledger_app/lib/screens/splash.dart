import 'dart:async';

import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/colorCodes.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';
import 'package:one_ledger_app/widgets/logo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    get();
    Timer(Duration(seconds: 5), () {
      Navigator.pushReplacementNamed(context, '/login');
      // if (getLoginStatus()) {
      //   Navigator.pushReplacementNamed(context, '/personalDetails');
      // } else {
      //   Navigator.pushReplacementNamed(context, '/login');
      // }
    });
  }

  get() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: splashScreenBG,
      body: Center(
        child: logo(),
      ),
    );
  }
}
