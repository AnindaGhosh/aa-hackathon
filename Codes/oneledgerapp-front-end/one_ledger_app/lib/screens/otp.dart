import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/otpModel.dart';
import 'package:one_ledger_app/utils/form_submissions/otpFormSubmit.dart';
import 'package:one_ledger_app/widgets/forms/otp_form.dart';
import 'package:one_ledger_app/widgets/headings.dart';
import 'package:one_ledger_app/widgets/loader.dart';
import 'package:one_ledger_app/widgets/logo.dart';

class Otp extends StatefulWidget {
  final String email;
  final String phoneNumber;

  Otp({this.email, this.phoneNumber});

  @override
  _OtpState createState() => _OtpState();
}

class _OtpState extends State<Otp> {
  final _otpFormKey = GlobalKey<FormState>();
  OtpModal _otpData;
  ValueNotifier<bool> loading = new ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 100,
                  ),
                  logo(),
                  SizedBox(
                    height: 30,
                  ),
                  heading(
                    title:
                        "Enter 6 digits verfication code sent to your mobile number",
                  ),
                  SizedBox(height: 30),
                  otpForm(
                    context: context,
                    onFormSubmit: (savedData) async {
                      if (loading.value == false) {
                        loading.value = true;
                        _otpData = savedData;
                        _otpData.email = widget.email;
                        _otpData.phone = widget.phoneNumber;
                        await otpCheck(context: context, otpData: _otpData);
                        loading.value = false;
                      }
                    },
                  ),
                ],
              ),
            ),
            ValueListenableBuilder(
              valueListenable: loading,
              builder: (BuildContext context, dynamic value, Widget child) {
                return value ? customLoader() : SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
