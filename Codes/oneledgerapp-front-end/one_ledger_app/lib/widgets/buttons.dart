import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/colorCodes.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

Widget submitButton({
  String buttonText,
  bool enabled,
  onTap,
}) {
  return Padding(
    padding: universalPadding,
    child: InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: enabled ? Color(0xff31AFC7) : Color(0xffEBECF0),
          borderRadius: BorderRadius.circular(25),
        ),
        child: Center(
          child: Padding(
            padding: universalPadding,
            child: Text(
              buttonText,
              style: enabled ? enabledbuttonTextStyle : disabledbuttonTextStyle,
            ),
          ),
        ),
      ),
    ),
  );
}

Widget backImageButton({BuildContext context}) {
  return InkWell(
    onTap: () {
      Navigator.pop(context);
    },
    child: Container(
      width: 30,
      height: 30,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/backButton.png"),
        ),
      ),
    ),
  );
}

Widget addButton({
  onTap,
}) {
  return Padding(
    padding: universalPadding,
    child: Container(
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: CircleAvatar(
            backgroundColor: Colors.white,
            child: Icon(
              Icons.add,
              color: splashScreenBG,
            ),
          ),
        ),
      ),
    ),
  );
}
