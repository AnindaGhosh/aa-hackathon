import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/kConstants.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

class BasicDateField extends StatelessWidget {
  final String hint;
  Function onSaved;

  BasicDateField({this.hint, this.onSaved});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: universalPadding,
        child: Container(
          child: DateTimeField(
            resetIcon: Icon(
              Icons.close,
              color: Color(0xff31AFC7),
            ),
            onSaved: onSaved,
            decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
                borderSide: BorderSide(
                  color: Color(0xffebecf0),
                  width: 2,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
                borderSide: BorderSide(
                  color: Color(0xff31AFC7),
                  width: 2,
                ),
              ),
              hintText: hint,
              hintStyle: hintTextStyle,
            ),
            format: format,
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(1700),
                initialDate: DateTime.now(),
                lastDate: DateTime.now(),
              );
            },
          ),
        ),
      ),
    );
  }
}
