import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

Widget linkText({String textContent, onTap}) {
  return InkWell(
    onTap: onTap,
    child: Text(
      textContent,
      style: linkTextStyle,
    ),
  );
}

Widget normalText({String textContent}) {
  return Text(
    textContent,
    style: normalTextStyle,
  );
}

Widget linkPlusNormalText(
    {String normalTextContent, String linkTextContent, onTap}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      normalText(textContent: normalTextContent),
      Text(" "),
      linkText(textContent: linkTextContent, onTap: onTap),
    ],
  );
}
