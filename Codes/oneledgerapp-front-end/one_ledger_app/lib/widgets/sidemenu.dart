import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/models/sqlit/userFinancialService.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

import '../constants/colorCodes.dart';
import '../constants/textStyles.dart';

String name;
String email;
bool associateSubscription = false;

class SideMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SideMenu();
  }
}

//create a list for the list items
class _SideMenu extends State<SideMenu> {
  UserDetails userDetails;
  List<UserFinancialService> financialServices;
  bool isLoading = true;
  @override
  void initState() {
    userDetails = fetchUseDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 2,
      child: Container(
        color: Colors.white,
        child: ListView(
          padding: const EdgeInsets.all(0.0),
          children: <Widget>[
            Container(
                child: UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.transparent),
              accountName: Text(
                userDetails.firstName + " " + userDetails.lastName,
                style: linkTextStyle,
              ),
              accountEmail: Text(
                userDetails.email,
                style: normalTextStyle,
              ),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                child: Image(
                  image: AssetImage("assets/logo/logo.png"),
                  fit: BoxFit.contain,
                ),
              ),
            )),
            listtiles(context,
                title: 'Banking',
                icon: FontAwesomeIcons.dollarSign,
                route: '/bankingPage'),
            listtiles(context,
                title: 'Mutual Fund',
                icon: FontAwesomeIcons.wallet,
                route: '/landingPage'),
            listtiles(context,
                title: 'EPF',
                icon: FontAwesomeIcons.shieldAlt,
                route: '/landingPage'),
            listtiles(context,
                title: 'Loans & Installments',
                icon: FontAwesomeIcons.wallet,
                route: '/landingPage'),
            listtiles(context,
                title: 'Settings', icon: Icons.settings, route: '/landingPage'),
            listtiles(context,
                title: 'Help',
                icon: FontAwesomeIcons.info,
                route: '/landingPage'),
            listtiles(context,
                title: 'Logout',
                icon: FontAwesomeIcons.signOutAlt,
                route: '/login'),
          ],
        ),
      ),
    );
  }
}

Widget listtiles(BuildContext context,
    {String title, IconData icon, route, onTap}) {
  return Column(
    children: <Widget>[
      ListTile(
        title: Text(title, style: normalTextStyle),
        leading: Icon(icon,
            size: MediaQuery.of(context).size.aspectRatio * 40.0,
            color: splashScreenBG),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: splashScreenBG,
          size: MediaQuery.of(context).size.aspectRatio * 40.0,
        ),
        onTap: () {
          if (route == null) {
            onTap();
          } else {
            Navigator.pop(context);
            Navigator.pushNamed(context, route);
          }
        },
      ),
      Divider(color: Colors.grey),
    ],
  );
}
