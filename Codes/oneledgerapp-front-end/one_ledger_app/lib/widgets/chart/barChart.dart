// import 'package:flutter/material.dart';
// import 'package:fl_chart/fl_chart.dart';
// import 'graphData.dart';

// class BarChartBuilder extends StatefulWidget {
//   const BarChartBuilder({
//     Key key,
//     @required this.screenWidth,
//   }) : super(key: key);

//   final double screenWidth;

//   @override
//   _BarChartBuilderState createState() => _BarChartBuilderState();
// }

// class _BarChartBuilderState extends State<BarChartBuilder> {
//   final Color barBackgroundColor = const Color(0xff72d8bf);
//   final Duration animDuration = Duration(milliseconds: 250);
//   int touchedIndex;

//   bool isPlaying = false;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.all(8),
//       decoration: BoxDecoration(
//         // color: Colors.black.withOpacity(0.1),
//         borderRadius: BorderRadius.circular(10),
//       ),
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 8.0),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             // buildTitleText(
//             //     context: context,
//             //     screenWidth: widget.screenWidth,
//             //     text: "Statistics"),
//             // buildSubTitleText(
//             //     context: context,
//             //     screenWidth: widget.screenWidth,
//             //     text: "1. Department Analysis"),
//             BarChartSample1(),
//           ],
//         ),
//       ),
//     );
//   }
// }

// int touchedIndex;

// class BarChartSample1 extends StatefulWidget {
//   final List<Color> availableColors = [
//     Colors.purpleAccent,
//     Colors.yellow,
//     Colors.lightBlue,
//     Colors.orange,
//     Colors.pink,
//     Colors.redAccent,
//   ];

//   @override
//   State<StatefulWidget> createState() => BarChartSample1State();
// }

// class BarChartSample1State extends State<BarChartSample1> {
//   @override
//   Widget build(BuildContext context) {
//     return AspectRatio(
//       aspectRatio: 1,
//       child: Card(
//         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//         color: const Color(0xff81e5cd),
//         child: Stack(
//           children: <Widget>[
//             Padding(
//               padding: const EdgeInsets.all(16),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 mainAxisSize: MainAxisSize.max,
//                 children: <Widget>[
//                   Text(
//                     'Statistics',
//                     style: TextStyle(
//                         color: const Color(0xff0f4a3c),
//                         fontSize: 24,
//                         fontWeight: FontWeight.bold),
//                   ),
//                   const SizedBox(
//                     height: 4,
//                   ),
//                   Text(
//                     'Department Analysis',
//                     style: TextStyle(
//                         color: const Color(0xff379982),
//                         fontSize: 18,
//                         fontWeight: FontWeight.bold),
//                   ),
//                   const SizedBox(
//                     height: 38,
//                   ),
//                   Expanded(
//                     child: Padding(
//                       padding: const EdgeInsets.symmetric(horizontal: 8.0),
//                       child: BarChart(
//                         mainBarData(),
//                       ),
//                     ),
//                   ),
//                   const SizedBox(
//                     height: 12,
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   BarChartGroupData makeGroupData(
//     int x,
//     double y, {
//     bool isTouched = false,
//     Color barColor = Colors.white,
//     double width = 22,
//     List<int> showTooltips = const [],
//   }) {
//     return BarChartGroupData(
//       x: x,
//       barRods: [
//         BarChartRodData(
//           y: isTouched ? y + 1 : y,
//           color: isTouched ? Colors.yellow : barColor,
//           width: width,
//           backDrawRodData: BackgroundBarChartRodData(
//             show: true,
//             y: 20,
//             color: barBackgroundColor,
//           ),
//         ),
//       ],
//       showingTooltipIndicators: showTooltips,
//     );
//   }

//   List<BarChartGroupData> showingGroups() =>
//       List.generate(instituteDepartment.length, (i) {
//         return departmentAnalysisData[i];
//       });

//   BarChartData mainBarData() {
//     return BarChartData(
//       barTouchData: BarTouchData(
//         touchTooltipData: BarTouchTooltipData(
//             tooltipBgColor: Colors.blueGrey,
//             getTooltipItem: (group, groupIndex, rod, rodIndex) {
//               return BarTooltipItem(
//                   departmentList[instituteDepartment[group.x.toInt()]]
//                           ["deptName"] +
//                       '\n' +
//                       (rod.y - 1).toInt().toString(),
//                   TextStyle(color: Colors.white));
//             }),
//       ),
//       titlesData: FlTitlesData(
//         show: true,
//         bottomTitles: SideTitles(
//           showTitles: true,
//           textStyle: TextStyle(
//               color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
//           margin: 16,
//           getTitles: (double value) {
//             return departmentList[instituteDepartment[value.floor()]]
//                 ["deptCode"];
//           },
//         ),
//         leftTitles: const SideTitles(
//           showTitles: false,
//         ),
//       ),
//       borderData: FlBorderData(
//         show: false,
//       ),
//       barGroups: showingGroups(),
//     );
//   }
// }
