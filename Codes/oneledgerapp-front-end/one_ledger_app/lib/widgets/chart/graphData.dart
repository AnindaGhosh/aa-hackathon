// //!Keep all the values related to graph in this file
// import 'package:fl_chart/fl_chart.dart';
// import 'package:flutter/material.dart';

// final Color barBackgroundColor = const Color(0xff72d8bf);

// var placedStudentCount = 0;
// var backLogStudentCount = 0;
// var experienceStudentCount = 0;
// var totalStudent = 0;

// List<BarChartGroupData> departmentAnalysisData = [];

// int femaleCount = 0, maleCount = 0;

// BarChartGroupData makeGroupData(
//   int x,
//   double y, {
//   bool isTouched = false,
//   Color barColor = Colors.white,
//   double width = 22,
//   List<int> showTooltips = const [],
// }) {
//   return BarChartGroupData(
//     x: x,
//     barRods: [
//       BarChartRodData(
//         y: isTouched ? y + 1 : y,
//         color: isTouched ? Colors.yellow : barColor,
//         width: width,
//         backDrawRodData: BackgroundBarChartRodData(
//           show: true,
//           y: 20,
//           color: barBackgroundColor,
//         ),
//       ),
//     ],
//     showingTooltipIndicators: showTooltips,
//   );
// }

// getGraphData({String year}) async {
//   placedStudentCount = 0;
//   femaleCount = 0;
//   maleCount = 0;
//   departmentAnalysisData = [];
//   backLogStudentCount = 0;
//   experienceStudentCount = 0;
//   totalStudent = 0;
//   if (year == "ALL") {
//     totalStudent = graphData == null ? 0 : graphData.length;
//     for (int i = 0; i < totalStudent; i++) {
//       if (graphData[i]['placement'] == '1') {
//         placedStudentCount++;
//       }
//       if (graphData[i]['gender'] == 'F') {
//         femaleCount++;
//       } else {
//         maleCount++;
//       }
//       if (graphData[i]['backlog'] != '0') {
//         backLogStudentCount++;
//       }
//       if (graphData[i]['workexp'] != '0') {
//         experienceStudentCount++;
//       }
//     }
//   } else {
//     for (int i = 0; i < graphData.length; i++) {
//       if (graphData[i]["batch"] == year) {
//         totalStudent++;
//       }
//       if (graphData[i]['placement'] == '1' && graphData[i]['batch'] == year) {
//         placedStudentCount++;
//       }
//       if (graphData[i]['gender'] == 'F' && graphData[i]['batch'] == year) {
//         femaleCount++;
//       } else {
//         maleCount++;
//       }
//       if (graphData[i]['backlog'] != '0' && graphData[i]['batch'] == year) {
//         backLogStudentCount++;
//       }
//       if (graphData[i]['workexp'] != '0' && graphData[i]['batch'] == year) {
//         experienceStudentCount++;
//       }
//     }
//   }

//   if (year == "ALL") {
//     for (int i = 0; i < instituteDepartment.length; i++) {
//       int temp = 0;
//       for (int j = 0; j < graphData.length; j++) {
//         //TODO:  add year for filter
//         if (graphData[j]['department'] == instituteDepartment[i]) {
//           temp++;
//         }
//       }
//       departmentAnalysisData
//           .add(makeGroupData(i, temp.toDouble(), isTouched: i == touchedIndex));
//       temp = 0;
//     }
//   } else {
//     for (int i = 0; i < instituteDepartment.length; i++) {
//       int temp = 0;
//       for (int j = 0; j < graphData.length; j++) {
//         //TODO:  add year for filter
//         if (graphData[j]['department'] == instituteDepartment[i] &&
//             graphData[j]["batch"] == year) {
//           temp++;
//         }
//       }
//       departmentAnalysisData
//           .add(makeGroupData(i, temp.toDouble(), isTouched: i == touchedIndex));
//       temp = 0;
//     }
//   }
//   // print(departmentAnalysisData);
// }
