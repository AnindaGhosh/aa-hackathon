import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

class ServicesCard extends StatefulWidget {
  String serviceName;
  String serviceId;
  bool status;
  Function onSaved;

  ServicesCard({this.serviceName, this.serviceId, this.status, this.onSaved});

  @override
  _ServicesCardState createState() => _ServicesCardState();
}

class _ServicesCardState extends State<ServicesCard> {
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: universalPadding,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            InkWell(
              onTap: () {
                if (widget.status == true) {
                  setState(() {
                    selected = !selected;
                  });
                  widget.onSaved(widget.serviceId);
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                  color: widget.status ? Colors.white : Color(0xffEBECF0),
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: selected ? Color(0xff31AFC7) : Color(0xffEBECF0),
                  ),
                ),
                child: Padding(
                  padding: universalPadding,
                  child: Text(
                    widget.serviceName,
                    style: selected ? linkTextStyle : hintTextStyle,
                  ),
                ),
              ),
            ),
            selected
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      FontAwesomeIcons.solidCheckCircle,
                      color: Colors.green,
                      size: 20,
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
