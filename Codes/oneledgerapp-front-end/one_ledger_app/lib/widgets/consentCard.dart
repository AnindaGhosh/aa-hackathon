import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

class ConsentCard extends StatefulWidget {
  String id;
  String requestFor;
  String startDate;
  String endDate;
  String frequency;
  String validity;
  String fetchingData;
  Function onSaved;

  ConsentCard({
    this.id,
    this.requestFor,
    this.startDate,
    this.endDate,
    this.frequency,
    this.validity,
    this.fetchingData,
    this.onSaved,
  });

  @override
  _ConsentCardState createState() => _ConsentCardState();
}

class _ConsentCardState extends State<ConsentCard> {
  bool selected = false;

  Widget rowColumnText(String text) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        text,
        style: normalTextStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: universalPadding,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  selected = !selected;
                });
                widget.onSaved(widget.id);
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: selected ? Color(0xff31AFC7) : Color(0xffEBECF0),
                  ),
                ),
                child: Padding(
                  padding: universalPadding,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          rowColumnText("Request For: ${widget.requestFor}"),
                          rowColumnText("Start Date: ${widget.startDate}"),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          rowColumnText("Frequency: ${widget.frequency}"),
                          rowColumnText("End Date: ${widget.endDate}"),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          rowColumnText("Valid For: ${widget.validity}"),
                          rowColumnText(
                              "Fetching Data: ${widget.fetchingData}"),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            selected
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      FontAwesomeIcons.solidCheckCircle,
                      color: Colors.green,
                      size: 20,
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
