import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/personalDetailsModel.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/datefield.dart';
import 'package:one_ledger_app/widgets/dropdown.dart';
import 'package:one_ledger_app/widgets/textfields.dart';

class PersonalDetailsForm extends StatefulWidget {
  Function onFormSubmit;
  PersonalDetailsForm({this.onFormSubmit});

  @override
  _PersonalDetailsFormState createState() => _PersonalDetailsFormState();
}

class _PersonalDetailsFormState extends State<PersonalDetailsForm> {
  var _personalDetailsformKey = GlobalKey<FormState>();
  PersonalDetailsModel inputData = PersonalDetailsModel();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _personalDetailsformKey,
      child: Column(
        children: <Widget>[
          textField(
              context: context,
              hint: "Enter your First Name",
              onSaved: (String value) {
                inputData.firstName = value;
              }),
          textField(
              context: context,
              hint: "Enter your Last Name",
              onSaved: (String value) {
                inputData.lastName = value;
              }),
          BasicDateField(
            hint: "Enter your DOB",
            onSaved: (value) {
              inputData.dateOfBirth = value;
            },
          ),
          dropDown(
              context: context,
              items: ["Male", "Female"],
              hint: "Enter Your Gender",
              value: inputData.gender,
              onChanged: (value) {
                setState(() {
                  inputData.gender = value;
                });
              }),
          textField(
            context: context,
            hint: "PAN (Optional)",
            onSaved: (String value) {
              inputData.panNumber = value;
            },
            obscureText: true,
          ),
          dropDown(
              context: context,
              items: ["3,10,000 - 5,00,000", "5,00,000-7,00,000"],
              hint: "Enter Your Salary Range",
              value: inputData.salarySlab,
              onChanged: (value) {
                setState(() {
                  inputData.salarySlab = value;
                });
              }),
          submitButton(
            buttonText: "Continue",
            enabled: true,
            onTap: () {
              _personalDetailsformKey.currentState.save();
              widget.onFormSubmit(inputData);
              print(inputData.toJson());
            },
          ),
        ],
      ),
    );
  }
}
