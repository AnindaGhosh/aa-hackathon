import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/consentCard.dart';

Widget consentTemplateForm({BuildContext context, key, onFormSubmit}) {
  ConsentTemplateModel inputData = ConsentTemplateModel();
  List<String> consentList = [];
  return Form(
    key: key,
    child: Column(
      children: <Widget>[
        ...productId.map(
          (e) => ConsentCard(
            endDate: "01/01/2020",
            startDate: "01/01/2020",
            fetchingData: e['consentTypes'],
            frequency: e['fetchType'],
            id: e['id'],
            onSaved: (val) {
              consentList.add(val);
            },
            requestFor: e['fiType'],
            validity: e['validity'],
          ),
        ),
        submitButton(
          buttonText: "Continue",
          enabled: true,
          onTap: () {
            inputData.consentList = consentList;
            onFormSubmit(inputData);
          },
        ),
      ],
    ),
  );
}
