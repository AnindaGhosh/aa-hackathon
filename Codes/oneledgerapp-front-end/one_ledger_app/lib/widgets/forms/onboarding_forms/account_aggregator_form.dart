import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/accountAggregatorModel.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/textfields.dart';

class AccountAggregatorForm extends StatefulWidget {
  Function onFormSubmit;
  AccountAggregatorForm({this.onFormSubmit});

  @override
  _AccountAggregatorFormState createState() => _AccountAggregatorFormState();
}

class _AccountAggregatorFormState extends State<AccountAggregatorForm> {
  var _accountAggregatorFormKey = GlobalKey<FormState>();
  AccountAggregatorModel inputData = AccountAggregatorModel();
  List<String> aaAccounts = [];
  List<Widget> aaFields = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _accountAggregatorFormKey,
      child: Column(
        children: <Widget>[
          textField(
              context: context,
              hint: "Enter your aa ID",
              keyBoardType: TextInputType.emailAddress,
              onSaved: (String value) {
                aaAccounts.add(value);
              }),
          Column(
            children: aaFields,
          ),
          addButton(onTap: () {
            aaFields.add(
              textField(
                  context: context,
                  hint: "Enter your aa ID",
                  keyBoardType: TextInputType.emailAddress,
                  onSaved: (String value) {
                    aaAccounts.add(value);
                  }),
            );
            setState(() {});
          }),
          submitButton(
            buttonText: "Continue",
            enabled: true,
            onTap: () {
              inputData.accounts = aaAccounts;
              _accountAggregatorFormKey.currentState.save();
              widget.onFormSubmit(inputData);
              print(inputData.toJson());
            },
          ),
        ],
      ),
    );
  }
}
