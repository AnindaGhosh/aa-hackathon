import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/financialServicesModel.dart';
import 'package:one_ledger_app/models/onboarding/personalDetailsModel.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/servicesCard.dart';

class FinancialServicesForm extends StatefulWidget {
  Function onFormSubmit;
  FinancialServicesForm({this.onFormSubmit});

  @override
  _FinancialServicesFormState createState() => _FinancialServicesFormState();
}

class _FinancialServicesFormState extends State<FinancialServicesForm> {
  var _financialServicesformKey = GlobalKey<FormState>();
  FinancialServicesModel inputData = FinancialServicesModel();
  List<String> optedServices = [];

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _financialServicesformKey,
      child: Column(
        children: <Widget>[
          ...services.map((e) => ServicesCard(
                serviceId: e['id'],
                serviceName: e['name'],
                status: e['status'],
                onSaved: (val) {
                  optedServices.add(val);
                },
              )),
          submitButton(
            buttonText: "Continue",
            enabled: true,
            onTap: () {
              inputData.services = optedServices;
              _financialServicesformKey.currentState.save();
              widget.onFormSubmit(inputData);
              print(inputData.toJson());
            },
          ),
        ],
      ),
    );
  }
}
