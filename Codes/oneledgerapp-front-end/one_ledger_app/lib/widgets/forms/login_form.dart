import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/models/loginModel.dart';
import 'package:one_ledger_app/utils/validators.dart';
import 'package:one_ledger_app/widgets/alerts.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/linkText.dart';
import 'package:one_ledger_app/widgets/textfields.dart';

Widget loginForm({BuildContext context, key, onFormSubmit}) {
  LoginModal inputData = LoginModal();
  return Form(
    key: key,
    child: Column(
      children: <Widget>[
        textField(
            context: context,
            hint: "Enter Email",
            keyBoardType: TextInputType.emailAddress,
            validator: validateEmail,
            onSaved: (String value) {
              inputData.email = value;
            }),
        textField(
          context: context,
          hint: "Enter Password",
          onSaved: (String value) {
            inputData.password = value;
          },
          obscureText: true,
        ),
        Padding(
          padding: universalPadding,
          child: Align(
            alignment: Alignment.centerRight,
            child: linkText(
              textContent: "Forgot Password?",
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return showPopup(
                        action1Text: "Yes",
                        action2Text: "No",
                        content: "You forgot your password?",
                        title: "Confirm?");
                  },
                );
              },
            ),
          ),
        ),
        submitButton(
          buttonText: "Sign In",
          enabled: true,
          onTap: () {
            onFormSubmit(inputData);
          },
        ),
        linkPlusNormalText(
          normalTextContent: "Don't have an account?",
          linkTextContent: "SignUp",
          onTap: () {
            Navigator.of(context).pushNamed('/signup');
          },
        ),
      ],
    ),
  );
}
