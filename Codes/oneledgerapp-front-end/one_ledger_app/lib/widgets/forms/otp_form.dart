import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/otpModel.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/otpfield.dart';

Widget otpForm({BuildContext context, key, onFormSubmit}) {
  OtpModal inputData = OtpModal();
  return Form(
    key: key,
    child: Column(
      children: <Widget>[
        otpField(
          context: context,
          onDone: (String passCode) {
            inputData.passcode = passCode;
            onFormSubmit(inputData);
          },
        ),
        submitButton(
          buttonText: "Confirm",
          enabled: true,
          onTap: () {
            print(inputData.toJson());
            // onFormSubmit(inputData);
          },
        ),
      ],
    ),
  );
}
