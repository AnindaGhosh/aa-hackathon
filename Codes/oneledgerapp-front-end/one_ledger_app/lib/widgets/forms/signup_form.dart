import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/registerModel.dart';
import 'package:one_ledger_app/utils/validators.dart';
import 'package:one_ledger_app/widgets/alerts.dart';
import 'package:one_ledger_app/widgets/buttons.dart';
import 'package:one_ledger_app/widgets/linkText.dart';
import 'package:one_ledger_app/widgets/textfields.dart';

Widget signUpForm({BuildContext context, key, onFormSubmit}) {
  SignUpModal inputData = SignUpModal();
  return Form(
    key: key,
    child: Column(
      children: <Widget>[
        textField(
          context: context,
          hint: "Enter Email",
          validator: validateEmail,
          keyBoardType: TextInputType.emailAddress,
          onSaved: (String value) {
            inputData.email = value;
          },
        ),
        textField(
          context: context,
          validator: validatePhone,
          hint: "Enter Phone Number",
          keyBoardType: TextInputType.number,
          onSaved: (String value) {
            inputData.phoneNumber = value;
          },
        ),
        textField(
          context: context,
          validator: validatePwd,
          hint: "Enter Password",
          obscureText: true,
          onSaved: (String value) {
            inputData.password = value;
          },
        ),
        textField(
          context: context,
          obscureText: true,
          hint: "Re-enter Password",
          onSaved: (String value) {
            inputData.reEnterPassword = value;
          },
        ),
        submitButton(
          buttonText: "Sign Up",
          enabled: true,
          onTap: () {
            onFormSubmit(inputData);
          },
        ),
        linkPlusNormalText(
          normalTextContent: "By creating an account you agree to our",
          linkTextContent: "Terms",
          onTap: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return aboutPopup();
              },
            );
          },
        ),
      ],
    ),
  );
}
