import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/padding.dart';

Widget logo() {
  return Padding(
    padding: universalPadding,
    child: Center(
      child: Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/logo/logo.png"),
          ),
        ),
      ),
    ),
  );
}
