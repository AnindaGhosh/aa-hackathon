import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

Widget dropDown(
    {BuildContext context, List items, onChanged, String hint, String value}) {
  return Container(
    width: MediaQuery.of(context).size.width,
    child: Padding(
      padding: universalPadding,
      child: Container(
        child: DropdownButtonFormField<String>(
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            border: InputBorder.none,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
              borderSide: BorderSide(
                color: Color(0xffebecf0),
                width: 2,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
              borderSide: BorderSide(
                color: Color(0xff31AFC7),
                width: 2,
              ),
            ),
            hintText: hint,
            hintStyle: hintTextStyle,
          ),
          items: items.map((e) {
            return DropdownMenuItem<String>(
              value: e,
              child: Text(e),
            );
          }).toList(),
          value: value,
          icon: Icon(
            Icons.keyboard_arrow_down,
            color: Color(0xff31AFC7),
          ),
          onChanged: onChanged,
        ),
      ),
    ),
  );
}
