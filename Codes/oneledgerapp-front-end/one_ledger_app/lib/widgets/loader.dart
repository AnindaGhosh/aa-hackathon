import 'package:flutter/material.dart';

Widget customLoader() {
  return Center(
    child: CircularProgressIndicator(),
  );
}
