import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

Widget heading({String title}) {
  return Text(
    title,
    textAlign: TextAlign.center,
    style: headingTextStyle,
  );
}

Widget onBoardingHeading({String title}) {
  return Text(
    title,
    textAlign: TextAlign.left,
    style: onBoardingHeadingTextStyle,
  );
}

Widget onBoardingSubHeading({String subHeading}) {
  return Text(
    subHeading,
    textAlign: TextAlign.left,
    style: onBoardingSubHeadingTextStyle,
  );
}
