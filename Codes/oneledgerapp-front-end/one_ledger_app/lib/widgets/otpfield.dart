import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

Widget otpField({BuildContext context, onDone}) {
  return Container(
    width: MediaQuery.of(context).size.width,
    child: Padding(
      padding: universalPadding,
      child: Center(
        child: PinCodeTextField(
          keyboardType: TextInputType.number,
          autofocus: true,
          defaultBorderColor: Color(0xffEBECF0),
          hasTextBorderColor: Color(0xff4CC9C5),
          pinBoxBorderWidth: 2,
          pinBoxRadius: 10,
          maxLength: 6,
          pinBoxHeight: 55,
          pinBoxWidth: 50,
          pinTextStyle: otpTextStyle,
          onDone: (value) {
            onDone(value);
          },
        ),
      ),
    ),
  );
}
