import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_ledger_app/constants/padding.dart';
import 'package:one_ledger_app/constants/textStyles.dart';

Widget textField({
  BuildContext context,
  String hint,
  TextEditingController controller,
  bool obscureText,
  TextInputType keyBoardType,
  validator,
  onSaved,
}) {
  return Container(
    width: MediaQuery.of(context).size.width,
    child: Padding(
      padding: universalPadding,
      child: Container(
        child: TextFormField(
          onSaved: onSaved,
          validator: validator,
          keyboardType: keyBoardType ?? TextInputType.text,
          controller: controller,
          obscureText: obscureText ?? false,
          style: fieldTextStyle,
          decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            border: InputBorder.none,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
              borderSide: BorderSide(
                color: Color(0xffebecf0),
                width: 2,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
              borderSide: BorderSide(
                color: Color(0xff31AFC7),
                width: 2,
              ),
            ),
            hintText: hint,
            hintStyle: hintTextStyle,
          ),
        ),
      ),
    ),
  );
}
