import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_awesome_alert_box/flutter_awesome_alert_box.dart';

Widget showPopup({
  String title,
  String content,
  String action1Text,
  String action2Text,
  Function action1,
  Function action2,
}) {
  return AlertDialog(
    actions: <Widget>[
      FlatButton(
        child: Text(action1Text),
        onPressed: action1,
      ),
      FlatButton(
        child: Text(action2Text),
        onPressed: action2,
      ),
    ],
    title: Text(title),
    content: Text(content),
  );
}

Widget aboutPopup() {
  return AboutDialog(
    applicationIcon: Image(
      image: AssetImage(
        "assets/logo/logo.png",
      ),
    ),
    applicationLegalese:
        "It's time to take charge of your financial goals and never miss any future opportunities. ",
    applicationName: "One Ledger Book",
    applicationVersion: "1.0",
  );
}

showSuccessAlert({BuildContext context, String title}) {
  SuccessAlertBoxCenter(
    context: context,
    messageText: title,
  );
}

showErrorAlert({BuildContext context, String title, String description}) {
  DangerAlertBox(
    context: context,
    title: title,
    messageText: description ?? "",
  );
}
