import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/models/transactionModel.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

String accountManagementUrl = baseUrl + "entitymanagement/";

Future<List<TransactionModel>> homepageAPI() async {
  String url = accountManagementUrl + "gettransactions/bank";

  UserDetails u = fetchUseDetails();
  Map data = {
    "uuid": u.uuid,
    "startTime": "1576722014",
    "endTime": "1577660204"
  };
  print(url);

  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(data),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        print("Status 1000");
        print(res['data'].toString());
        List<TransactionModel> txns;
        for (int i = 0; i < res['data'][0].length; i++) {
          txns[i] = TransactionModel.fromJson(res['data'][0][i]);
        }
        return txns;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    return [];
  }
  return [];
}
