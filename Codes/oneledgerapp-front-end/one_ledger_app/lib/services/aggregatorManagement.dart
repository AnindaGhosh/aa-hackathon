import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:one_ledger_app/models/onboarding/consentStatus.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/models/sqlit/account.dart';
import 'package:one_ledger_app/models/sqlit/consentDetails.dart';
import 'package:one_ledger_app/models/sqlit/profile.dart';
import 'package:one_ledger_app/models/sqlit/summary.dart';
import 'package:one_ledger_app/repository/databaseRepository/accountDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/consentDetailDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/profileDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/summaryDatabaseRepository.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

String aaManagementUrl = baseUrl + "aggregatormanagement/";
Future<int> requestConsentAPI(
    {ConsentTemplateModel consentTemplateData}) async {
  String url = aaManagementUrl + "requestconsent";
  print(url);
  print(consentTemplateData.toJson());
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(consentTemplateData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        print("Status 1000");
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    return 0;
  }
  return 1;
}

Future<ConsentStatus> getConsentStatusAPI(
    {ConsentTemplateModel consentTemplateData}) async {
  String url = aaManagementUrl + "getconsentstatus";
  print(url);
  print(consentTemplateData.toJson());
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(consentTemplateData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        print("Status 1000");
        List responseData = res['data'];
        ConsentStatus consentStatus = new ConsentStatus();
        int countMandatoryConsent = 0;
        consentStatus.active = 0;
        consentStatus.total = 0;
        consentStatus.mandatoryConsentsApproved = false;
        for (int i = 0; i < responseData.length; i++) {
          for (int j = 0; j < responseData[i]['data'].length; j++) {
            if (responseData[i]['data'][j]['status'] == 'ACTIVE') {
              consentStatus.active++;
            }
            consentStatus.total++;
          }
          if (mandatoryConsents.contains(responseData[i]['ConsentTypeID']) &&
              consentStatus.active == consentStatus.total) {
            countMandatoryConsent++;
          }
        }
        if (countMandatoryConsent == mandatoryConsents.length)
          consentStatus.mandatoryConsentsApproved = true;
        return consentStatus;
      } else {
        return null;
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    return null;
  }
  return null;
}

Future<List<String>> getConsentsAPI(
    {ConsentTemplateModel consentTemplateData}) async {
  String url = aaManagementUrl + "getconsentlist";
  print(url);
  print(consentTemplateData.toJson());
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(consentTemplateData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      List<String> consentList = [];
      if (res['code'] == 1000) {
        print("Status 1000");
        DatabaseProvider databaseProvider = DatabaseProvider.get;
        ConsentDetailDatabaseRepository consentDetailDRepo =
            new ConsentDetailDatabaseRepository(databaseProvider);
        AccountDatabaseRepository accountDRepo =
            new AccountDatabaseRepository(databaseProvider);
        List responseList = res['data'];
        List<Account> accountList = [];
        for (int i = 0; i < responseList.length; i++) {
          for (int j = 0; j < responseList[i]['data'].length; j++) {
            ConsentDetail consentDetail =
                ConsentDetail.fromJson(responseList[i]['data'][j]);
            consentDetail.id =
                (new DateTime.now().millisecondsSinceEpoch % 1000);
            consentDetail.productId = responseList[i]['ConsentTypeID'];
            consentDetail.userId = getCurrentUserId();
            ConsentDetail savedConsentDetail =
                await consentDetailDRepo.insert(consentDetail);
            consentList.add(consentDetail.consentId);
            Iterable list = responseList[i]['data'][j]['accounts'];
            accountList
                .addAll(list.map((model) => Account.fromJson(model)).toList());
            for (int k = 0; k < accountList.length; k++) {
              accountList[k].id =
                  (new DateTime.now().millisecondsSinceEpoch % 1000);
              accountList[k].consentDetailId = savedConsentDetail.id;
              try {
                await accountDRepo.insert(accountList[k]);
              } catch (e) {
                print(e);
              }
            }
            print("New consentDetail has been added in DB");
          }
        }
        return consentList;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call " + e);
    return [];
  }
  return [];
}

Future<int> getProfileAndSummaryAPI({String consentId}) async {
  String url = aaManagementUrl + "getprofilesummary";
  print(url);
  Map<String, dynamic> requestBody = {"consentId": consentId};
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(requestBody),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        print("Status 1000");
        DatabaseProvider databaseProvider = DatabaseProvider.get;
        ProfileDatabaseRepository profileDRepo =
            new ProfileDatabaseRepository(databaseProvider);
        SummaryDatabaseRepository summaryDRepo =
            new SummaryDatabaseRepository(databaseProvider);
        Summary summary = new Summary();
        Profile profile = new Profile();
        List responseData = res['data'];
        for (int i = 0; i < responseData.length; i++) {
          String fipId = fipNames.keys.firstWhere(
              (k) => fipNames[k] == responseData[i]['bankName'],
              orElse: () => null);
          profile =
              Profile.fromJson(responseData[i]['profile']['holderInformation']);
          profile.id = (new DateTime.now().millisecondsSinceEpoch % 1000000);
          profile.fipId = fipId;
          profile.userId = getCurrentUserId();
          (profileDRepo.insert(profile))
              .then((value) => print("New profile has been added in DB"))
              .catchError((e) => print(e));
          summary = Summary.fromJson(responseData[i]['summary']);
          summary.id = (new DateTime.now().millisecondsSinceEpoch % 1000000);
          summary.fipId = fipId;
          summary.userId = getCurrentUserId();
          (summaryDRepo.insert(summary))
              .then((value) => print("New summary has been added in DB"))
              .catchError((e) => print(e));
        }
        //profile.fromJson(res);
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    return 0;
  }
  return 1;
}

Future<int> fetchAndSaveProfileAndSummary({List<String> consentList}) async {
  int n = 0;
  int count = 0;
  for (int i = 0; i < consentList.length; i++) {
    n = await getProfileAndSummaryAPI(consentId: consentList[i]);
    if (n == 1) count++;
  }
  return count;
}
