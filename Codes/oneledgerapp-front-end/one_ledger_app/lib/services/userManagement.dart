import 'dart:convert';
import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:one_ledger_app/models/accesTokens.dart';
import 'package:one_ledger_app/models/loginModel.dart';
import 'package:one_ledger_app/models/otpModel.dart';
import 'package:one_ledger_app/models/registerModel.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';
import 'package:one_ledger_app/repository/databaseRepository/userDetailDatabaseRepository.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

String userManagementUrl = baseUrl + "usermanagement/";

Future<int> loginAPI({LoginModal loginData}) async {
  String url = userManagementUrl + "signin";
  print(url);
  int returnCode;
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(loginData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        DatabaseProvider databaseProvider = DatabaseProvider.get;
        UserDetailDatabaseRepository userDRepo =
            new UserDetailDatabaseRepository(databaseProvider);
        (userDRepo.getDetailByEmail(loginData.email)).then((userDetails) =>
            setUserBasicInformation(userDetails.id, userDetails.email,
                userDetails.phoneNumber, userDetails.uuid));
        accessTokens = AccessTokens.fromJson(res['data'][0]);
        if (res['data'][0]['onboardStatus'] == true) {
          makeUserOnboard();
        }
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    returnCode = 0;
    return 0;
  }
  return returnCode;
}

Future<int> signUpAPI({SignUpModal signUpData}) async {
  String url = userManagementUrl + "signup";
  int returnCode;
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(signUpData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        Random random = new Random();
        int randId = random.nextInt(1000) + 10;
        DatabaseProvider databaseProvider = DatabaseProvider.get;
        //Database db = await databaseProvider.db();
        UserDetailDatabaseRepository userDRepo =
            new UserDetailDatabaseRepository(databaseProvider);
        UserDetails userDetails = new UserDetails();
        userDetails.id = randId;
        userDetails.email = signUpData.email;
        userDetails.phoneNumber = signUpData.phoneNumber;
        userDetails.uuid = res['data'][0]['uuid'];
        (userDRepo.insert(userDetails))
            .then((userDetails) => print("Stored user details in local DB"))
            .catchError((e) => print(e));
        UserDetails u = await userDRepo.getDetailByEmail(userDetails.email);
        //print(u.toJson());
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    returnCode = 0;
    return 0;
  }
  return returnCode;
}

Future<int> otpAPI({OtpModal otpData}) async {
  String url = userManagementUrl + "confirmsignup";
  int returnCode;
  print(otpData.toJson());
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(otpData.toJson()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    print(response.body);
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        setUserBasicInformation(
            getCurrentUserId(), otpData.email, otpData.phone, res['uuid']);
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    returnCode = 400;
  }
  return returnCode;
}
