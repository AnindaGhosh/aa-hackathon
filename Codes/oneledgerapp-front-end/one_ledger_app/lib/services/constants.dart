import 'package:one_ledger_app/models/accesTokens.dart';
import 'package:shared_preferences/shared_preferences.dart';

String baseUrl = "https://api.oneledgerbook.com/";

List<String> aggregatorList = ["onemoney"];

Map responseCodes = {
  0: "No Internet Connection",
  1001: "Incorrect Password Format",
  1002: "InvalidParameterException",
  1003: "ExpiredCodeException",
  1004: "OTP did not match. Please Try Again.!",
  1005: "Incorrect Email/Password",
  1006: "Unable to resend OTP",
  1000: "Success",
  1019: "Oops something went wrong. Please try again later.!"
};

List productId = [
  {
    "id": "OLBOTBANK00",
    "code": "ONETIME-PROFILE-1DA-BANK",
    "fiType": "DEPOSIT",
    "validity": "1 day",
    "fetchType": "ONETIME",
    "dataFetch": "1 hour",
    "consentTypes": "Profile, Summary"
  },
  {
    "id": "OLBOTBANK01",
    "code": "ONETIME-TXN-6MN-BANK",
    "fiType": "DEPOSIT",
    "validity": "1 day",
    "fetchType": "ONETIME",
    "dataFetch": "6 months",
    "consentTypes": "Transactions"
  },
  {
    "id": "OLBPEBANK01",
    "code": "PERIODIC-TXN-1HR-BANK",
    "fiType": "DEPOSIT",
    "validity": "1 Year",
    "fetchType": "PERIODIC",
    "dataFetch": "1 hour",
    "consentTypes": "Transactions"
  }
];
List services = [
  {"id": "BANK", "name": "Banking", "status": true},
  {"id": "RD", "name": "Recurring Deposits", "status": true},
  {"id": "FD", "name": "Fixed Deposits", "status": true},
  {"id": "MUTUAL_FUNDS", "name": "Mutual Funds", "status": true},
  {"id": "EPF", "name": "Employee Provident Fund", "status": false},
  {"id": "NPS", "name": "National Pension Scheme", "status": false},
  {"id": "LOAN_INSTALLMENT", "name": "Loans and Installments", "status": false},
  {
    "id": "OTHER_INVESTMENT",
    "name": "Other Investment Management",
    "status": false
  }
];

List<String> mandatoryConsents = ["OLBOTBANK00", "OLBOTBANK01", "OLBPEBANK01"];

Map fipNames = {"finsharebank": "Universal Bank", "axisbank": "Axis Bank"};

AccessTokens accessTokens;

SharedPreferences prefs;
