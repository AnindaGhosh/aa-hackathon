import 'package:flutter/cupertino.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/widgets/alerts.dart';

void handleErrorResponseCodes({BuildContext context, int code}) {
  String errorMessage = responseCodes[code];
  print(errorMessage);
  showErrorAlert(
      context: context, title: errorMessage, description: "Please Try Again");
}
