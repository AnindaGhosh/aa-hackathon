import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

String accountManagementUrl = baseUrl + "accountmanagement/";

Future<int> callOnboardingAPI({Map<String, dynamic> onboarding}) async {
  String url = accountManagementUrl + "onboard";
  print(url);
  print(onboarding);
  try {
    http.Response response = await http.post(
      url,
      body: jsonEncode(onboarding),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'AccessToken': accessTokens.accessToken
      },
    );
    if (response.statusCode == 200) {
      Map res = json.decode(response.body);
      if (res['code'] == 1000) {
        print("Status 1000");
        return 1;
      } else {
        return res['code'];
      }
    }
  } catch (e) {
    print("Unsuccessful call");
    return 0;
  }
  return 1;
}
