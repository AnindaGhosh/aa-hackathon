import 'package:flutter/material.dart';
import 'package:one_ledger_app/screens/bankScreen.dart';
import 'package:one_ledger_app/screens/homepage.dart';
import 'package:one_ledger_app/screens/login.dart';
import 'package:one_ledger_app/screens/onboarding/account_aggregator.dart';
import 'package:one_ledger_app/screens/onboarding/financial_services.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/screens/onboarding/personal_details.dart';
import 'package:one_ledger_app/screens/onboarding/consent_template.dart';
import 'package:one_ledger_app/screens/otp.dart';
import 'package:one_ledger_app/screens/signup.dart';
import 'package:one_ledger_app/screens/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'One Ledger Book',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
      routes: {
        '/login': (context) => Login(),
        '/signup': (context) => SignUp(),
        '/otp': (context) => Otp(),
        '/landingPage': (context) => HomePage(),
        '/bankingPage': (context) => BankScreen(),
        '/personalDetails': (context) => PersonalDetails(),
        '/financialServices': (context) => FinancialServices(),
        '/consentTemplate': (context) => ConsentTemplate(),
        '/aa': (context) => AccountAggregator(),
        '/loadingPage': (context) => LoadingPage(),
      },
    );
  }
}
