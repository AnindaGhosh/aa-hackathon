class ConsentStatus {
  int active;
  int total;
  bool mandatoryConsentsApproved;

  ConsentStatus({this.active, this.total, this.mandatoryConsentsApproved});
}
