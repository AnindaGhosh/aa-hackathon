class FinancialServicesModel {
  List<String> services;

  FinancialServicesModel({
    this.services,
  });

  Map<String, dynamic> toJson() {
    return {
      "services": this.services,
    };
  }
}
