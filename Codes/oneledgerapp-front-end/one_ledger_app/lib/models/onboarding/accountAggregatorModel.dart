class AccountAggregatorModel {
  List accounts;

  AccountAggregatorModel({
    this.accounts,
  });

  Map<String, dynamic> toJson() {
    return {
      "accounts": this.accounts,
    };
  }
}
