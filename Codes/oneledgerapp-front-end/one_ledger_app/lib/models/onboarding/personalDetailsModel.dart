class PersonalDetailsModel {
  String firstName;
  String lastName;
  DateTime dateOfBirth;
  String gender;
  String panNumber;
  String salarySlab;

  PersonalDetailsModel({
    this.firstName,
    this.lastName,
    this.dateOfBirth,
    this.gender,
    this.panNumber,
    this.salarySlab,
  });

  Map<String, dynamic> toJson() {
    return {
      "firstName": this.firstName,
      "lastName": this.lastName,
      "dateOfBirth": this.dateOfBirth,
      "gender": this.gender,
      "pan": this.panNumber,
      "salarySlab": this.salarySlab
    };
  }
}
