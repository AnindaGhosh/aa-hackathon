class ConsentTemplateModel {
  String phoneNumber;
  String email;
  String aggregator;
  List<String> consentList;

  ConsentTemplateModel(
      {this.email, this.phoneNumber, this.aggregator, this.consentList});

  Map<String, dynamic> toJson() {
    return {
      "phonenumber": phoneNumber,
      "aggregator": aggregator,
      "consent_types": consentList,
    };
  }
}
