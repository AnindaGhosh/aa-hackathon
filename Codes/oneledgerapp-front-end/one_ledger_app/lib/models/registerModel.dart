class SignUpModal {
  String email;
  String password;
  String phoneNumber;
  String reEnterPassword;

  SignUpModal({
    this.email,
    this.password,
    this.phoneNumber,
    this.reEnterPassword,
  });

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
      'phone_number': "+91" + phoneNumber,
    };
  }
}
