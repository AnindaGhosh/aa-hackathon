import 'package:flutter/cupertino.dart';

class BarChartData {
  final String year;

  final double count;

  final Color barColor;

  BarChartData({
    @required this.year,
    @required this.count,
    @required this.barColor,
  });
}
