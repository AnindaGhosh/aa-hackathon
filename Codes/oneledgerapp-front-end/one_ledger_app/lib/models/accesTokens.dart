class AccessTokens {
  String accessToken;
  String tokenId;
  String refreshToken;

  AccessTokens({this.accessToken, this.tokenId, this.refreshToken});

  factory AccessTokens.fromJson(Map data) {
    return AccessTokens(
        accessToken: data['accessToken'],
        refreshToken: data['refreshToken'],
        tokenId: data['tokenId']);
  }
}
