class TransactionModel {
  int valueDate;
  String fipId;
  String mode;
  List<String> labels;
  String txnId;
  String reference;
  String fipName;
  double balance;
  int transactionTimestamp;
  List<String> category;
  double amount;
  String uuid;
  String fipType;
  String narration;
  String type;

  TransactionModel.fromJson(Map<String, dynamic> json)
      : this.valueDate =
            DateTime.parse(json['valueDate']).millisecondsSinceEpoch,
        this.fipId = json['fipId'],
        this.mode = json['mode'],
        this.labels = (json['label']).map((label) => label as String).toList(),
        this.txnId = json['txnId'],
        this.reference = json['reference'],
        this.fipName = json['fipName'],
        this.balance = double.parse(json['balance']),
        this.transactionTimestamp = json['transactionTimestamp'],
        this.category =
            (json['category']).map((category) => category as String).toList(),
        this.amount = double.parse(json['amount']),
        this.uuid = json['uuid'],
        this.fipType = json['fipType'],
        this.narration = json['narration'],
        this.type = json['type'];
}
