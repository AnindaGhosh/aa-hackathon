import 'dart:ffi';

class Summary {
  int id;
  int userId;
  String fipId;
  int balanceDateTime;
  String branch;
  String currency;
  double currentBalance;
  double currentODLimit;
  double drawingLimit;
  String exchgeRate;
  String facility;
  String micrCode;
  int openingDate;
  int pendingAmount;
  String pendingTransactionType;
  String status;
  String type;

  Summary();

  Map<String, dynamic> toJson() {
    return {
      "balanceDateTime":
          DateTime.fromMillisecondsSinceEpoch(this.balanceDateTime)
              .toIso8601String(),
      "branch": this.branch,
      "currency": this.currency,
      "currentBalance": this.currentBalance.toString(),
      "currentODLimit": this.currentODLimit.toString(),
      "drawingLimit": this.drawingLimit.toString(),
      "exchgeRate": this.exchgeRate,
      "facility": this.facility,
      "micrCode": this.micrCode,
      "openingDate": DateTime.fromMillisecondsSinceEpoch(this.openingDate)
          .toIso8601String()
          .substring(0, 10),
      "Pending.amount": this.pendingAmount,
      "Pending.transactionType": this.pendingTransactionType,
      "status": this.status,
      "type": this.type
    };
  }

  Summary.fromJson(Map<String, dynamic> json)
      : this.balanceDateTime =
            DateTime.parse(json['balanceDateTime']).millisecondsSinceEpoch,
        this.branch = json['branch'],
        this.currency = json['currency'],
        this.currentBalance = double.parse(json['currentBalance']),
        this.currentODLimit = double.parse(json['currentODLimit']),
        this.drawingLimit = double.parse(json['drawingLimit']),
        this.exchgeRate = json['exchgeRate'],
        this.facility = json['facility'],
        this.micrCode = json['micrCode'],
        this.openingDate =
            DateTime.parse(json['openingDate']).millisecondsSinceEpoch,
        this.id = json["id"],
        this.pendingAmount = json['Pending.amount'],
        this.pendingTransactionType = json['Pending.transactionType'],
        this.status = json['status'],
        this.type = json['type'];
}
