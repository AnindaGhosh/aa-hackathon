import 'account.dart';

class ConsentDetail {
  int id;
  int userId;
  String consentId;
  String consentHandle;
  String productId;
  String status;
  String accountId;
  String aaId;
  String vua;
  int consentCreationDate;
  List<Account> accounts;

  ConsentDetail();

  Map<String, dynamic> toJson() {
    return {
      "consentID": this.consentId,
      "consentHandle": this.consentHandle,
      "status": this.status,
      "productID": this.productId,
      "accountID": this.accountId,
      "aaId": this.aaId,
      "vua": this.vua,
      "consentCreationData":
          (DateTime.fromMillisecondsSinceEpoch(this.consentCreationDate)
                  .toIso8601String())
              .substring(0, 10)
    };
  }

  ConsentDetail.fromJson(Map<String, dynamic> json)
      : this.consentId = json['consentID'],
        this.consentHandle = json['consentHandle'],
        this.productId = json['productID'],
        this.status = json['status'],
        this.accountId = json['accountID'],
        this.aaId = json['aaId'],
        this.vua = json['vua'],
        this.consentCreationDate =
            DateTime.parse(json['consentCreationData']).millisecondsSinceEpoch;
}
