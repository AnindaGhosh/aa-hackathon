class Profile {
  int id;
  int userId;
  String fipId;
  String address;
  String ckycCompliance;
  int dob;
  String email;
  String landline;
  String mobile;
  String name;
  String nominee;
  String pan;

  Profile();

  Map<String, dynamic> toJson() {
    return {
      "fipId": this.fipId,
      "address": this.address,
      "ckycCompliance": this.ckycCompliance,
      "dob": (DateTime.fromMillisecondsSinceEpoch(this.dob).toIso8601String())
          .substring(0, 10),
      "email": this.email,
      "landline": this.landline,
      "mobile": this.mobile,
      "name": this.name,
      "nominee": this.nominee,
      "pan": this.pan
    };
  }

  Profile.fromJson(Map<String, dynamic> json)
      : this.address = json['address'],
        this.dob = DateTime.parse(json['dob']).millisecondsSinceEpoch,
        this.ckycCompliance = json['ckycCompliance'],
        this.email = json['email'],
        this.landline = json['landline'],
        this.mobile = json['mobile'],
        this.name = json['name'],
        this.nominee = json['nominee'],
        this.pan = json['pan'];
}
