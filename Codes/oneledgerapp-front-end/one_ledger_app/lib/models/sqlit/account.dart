class Account {
  int id;
  int consentDetailId;
  String fipName;
  String fipId;
  String accountType;
  String linkReferenceNumber;
  String maskedAccountNumber;
  String fiType;

  Account();

  Map<String, dynamic> toJson() {
    return {
      "fipName": this.fipName,
      "fipId": this.fipId,
      "accountType": this.accountType,
      "linkReferenceNumber": this.linkReferenceNumber,
      "maskedAccountNumber": this.maskedAccountNumber,
      "fiType": this.fiType
    };
  }

  Account.fromJson(Map<String, dynamic> json)
      : fipName = json['fipName'],
        fipId = json['fipId'],
        accountType = json['accountType'],
        linkReferenceNumber = json['linkReferenceNumber'],
        maskedAccountNumber = json['maskedAccountNumber'],
        fiType = json['fiType'];
}
