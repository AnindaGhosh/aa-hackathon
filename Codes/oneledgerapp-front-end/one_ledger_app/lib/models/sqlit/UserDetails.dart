class UserDetails extends Object {
  int id;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  int dob;
  String gender;
  String pan;
  String incomeRange;
  String uuid;

  UserDetails();
  /* UserDetails(this.firstName, this.lastName, this.email, this.phoneNumber,
      this.dob, this.gender, this.pan, this.incomeRange); */

  Map<String, dynamic> toJson() {
    return {
      "firstName": this.firstName,
      "lastName": this.lastName,
      "email": this.email,
      "phoneNumber": this.phoneNumber,
      "dob": DateTime.fromMillisecondsSinceEpoch(this.dob)
          .toIso8601String()
          .substring(0, 10),
      "gender": this.gender,
      "pan": this.pan,
      "salarySlab": this.incomeRange
    };
  }
}
