class LoginModal {
  String email;
  String password;

  LoginModal({this.email, this.password});

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
    };
  }
}
