class OtpModal {
  String email;
  String phone;
  String passcode;

  OtpModal({this.email, this.phone, this.passcode});

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'phnumber': "+91" + phone,
      'passcode': passcode,
    };
  }
}
