import 'dart:async';
import 'package:one_ledger_app/dao/profileDao.dart';
import 'package:one_ledger_app/models/sqlit/profile.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class ProfileDatabaseRepository {
  final dao = ProfileDao();

  DatabaseProvider databaseProvider;

  ProfileDatabaseRepository(this.databaseProvider);

  Future<Profile> insert(Profile profile) async {
    final db = await databaseProvider.db();
    profile.id = await db.insert(dao.tableName, dao.toMap(profile));
    return profile;
  }

  Future<Profile> delete(Profile profile) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [profile.id]);
    return profile;
  }

  Future<Profile> update(Profile profile) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(profile),
        where: dao.columnId + " = ?", whereArgs: [profile.id]);
    return profile;
  }

  Future<List<Profile>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<Profile>> getDetailsGroupByFipId(int userId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'user_id = ?', whereArgs: [userId], groupBy: 'fip_id');
    return dao.fromList(maps);
  }

  Future<Profile> getSummaryByFipId(int userId, String fipId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.rawQuery(
        'SELECT * FROM profile WHERE user_id=? and fip_id=?', [userId, fipId]);
    return dao.fromMap(maps.first);
  }
}
