import 'dart:async';
import 'package:one_ledger_app/dao/userFinancialServicesDao.dart';
import 'package:one_ledger_app/models/sqlit/userFinancialService.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class UserFinancialServicesDatabaseRepository {
  final dao = UserFinancialServicesDao();

  DatabaseProvider databaseProvider;

  UserFinancialServicesDatabaseRepository(this.databaseProvider);

  Future<UserFinancialService> insert(
      UserFinancialService userFinancialServices) async {
    final db = await databaseProvider.db();
    userFinancialServices.id =
        await db.insert(dao.tableName, dao.toMap(userFinancialServices));
    return userFinancialServices;
  }

  Future<UserFinancialService> delete(
      UserFinancialService userFinancialService) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [userFinancialService.id]);
    return userFinancialService;
  }

  Future<UserFinancialService> update(
      UserFinancialService userFinancialService) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(userFinancialService),
        where: dao.columnId + " = ?", whereArgs: [userFinancialService.id]);
    return userFinancialService;
  }

  Future<List<UserFinancialService>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<UserFinancialService>> getDetailByUserId(String userId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db
        .query(dao.tableName, where: "user_id = ?", whereArgs: [userId]);
    return dao.fromList(maps);
  }
}
