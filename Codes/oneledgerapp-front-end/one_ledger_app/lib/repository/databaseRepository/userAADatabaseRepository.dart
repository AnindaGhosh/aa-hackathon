import 'dart:async';
import 'package:one_ledger_app/dao/userRegisteredAccountAggregratorDao.dart';
import 'package:one_ledger_app/models/sqlit/userRegisteredAccountAggregrator.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class UserAADatabaseRepository {
  final dao = UserRegisteredAccountAggregratorDao();

  DatabaseProvider databaseProvider;

  UserAADatabaseRepository(this.databaseProvider);

  Future<UserRegisteredAccountAggregrator> insert(
      UserRegisteredAccountAggregrator userAA) async {
    final db = await databaseProvider.db();
    userAA.id = await db.insert(dao.tableName, dao.toMap(userAA));
    return userAA;
  }

  Future<UserRegisteredAccountAggregrator> delete(
      UserRegisteredAccountAggregrator userAA) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [userAA.id]);
    return userAA;
  }

  Future<UserRegisteredAccountAggregrator> update(
      UserRegisteredAccountAggregrator userAA) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(userAA),
        where: dao.columnId + " = ?", whereArgs: [userAA.id]);
    return userAA;
  }

  Future<List<UserRegisteredAccountAggregrator>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<UserRegisteredAccountAggregrator>> getDetailByUserId(
      int userId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db
        .query(dao.tableName, where: "user_id = ?", whereArgs: [userId]);
    return dao.fromList(maps);
  }
}
