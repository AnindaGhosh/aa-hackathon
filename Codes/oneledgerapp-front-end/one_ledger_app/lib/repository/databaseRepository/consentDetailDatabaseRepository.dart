import 'dart:async';
import 'package:one_ledger_app/dao/consentDetailsDao.dart';
import 'package:one_ledger_app/models/sqlit/consentDetails.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class ConsentDetailDatabaseRepository {
  final dao = ConsentDetailsDao();

  DatabaseProvider databaseProvider;

  ConsentDetailDatabaseRepository(this.databaseProvider);

  Future<ConsentDetail> insert(ConsentDetail consentDetail) async {
    final db = await databaseProvider.db();
    consentDetail.id = await db.insert(dao.tableName, dao.toMap(consentDetail));
    return consentDetail;
  }

  Future<ConsentDetail> delete(ConsentDetail consentDetail) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [consentDetail.id]);
    return consentDetail;
  }

  Future<ConsentDetail> update(ConsentDetail consentDetail) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(consentDetail),
        where: dao.columnId + " = ?", whereArgs: [consentDetail.id]);
    return consentDetail;
  }

  Future<List<ConsentDetail>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<ConsentDetail>> getDetailsGroupByConsentId(int userId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'user_id = ?', whereArgs: [userId], groupBy: 'consent_id');
    return dao.fromList(maps);
  }

  Future<ConsentDetail> getConsentDetailByConsentId(String consentId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'consent_id = ?', whereArgs: [consentId], limit: 1);
    return dao.fromMap(maps.first);
  }
}
