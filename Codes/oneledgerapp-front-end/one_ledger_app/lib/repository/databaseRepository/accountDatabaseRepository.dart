import 'dart:async';
import 'package:one_ledger_app/dao/accountDao.dart';
import 'package:one_ledger_app/models/sqlit/account.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class AccountDatabaseRepository {
  final dao = AccountDao();

  DatabaseProvider databaseProvider;

  AccountDatabaseRepository(this.databaseProvider);

  Future<Account> insert(Account account) async {
    final db = await databaseProvider.db();
    account.id = await db.insert(dao.tableName, dao.toMap(account));
    return account;
  }

  Future<Account> delete(Account account) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [account.id]);
    return account;
  }

  Future<Account> update(Account account) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(account),
        where: dao.columnId + " = ?", whereArgs: [account.id]);
    return account;
  }

  Future<List<Account>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<Account>> getDetailsGroupByConsentDetailId() async {
    final db = await databaseProvider.db();
    List<Map> maps =
        await db.query(dao.tableName, groupBy: 'consent_detail_id');
    return dao.fromList(maps);
  }

  Future<List<Account>> getAccountListByConsentDetailId(
      int consentDetailId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'consent_detail_id = ?', whereArgs: [consentDetailId]);
    return dao.fromList(maps);
  }

  Future<Account> getAccountByConsentDetailId(
      int consentDetailId, String linkReferenceNumber) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'consent_detail_id = ? and link_reference_number = ?',
        whereArgs: [consentDetailId, linkReferenceNumber]);
    return dao.fromMap(maps.first);
  }
}
