import 'dart:async';
import 'package:one_ledger_app/dao/summaryDao.dart';
import 'package:one_ledger_app/models/sqlit/summary.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class SummaryDatabaseRepository {
  final dao = SummaryDao();

  DatabaseProvider databaseProvider;

  SummaryDatabaseRepository(this.databaseProvider);

  Future<Summary> insert(Summary summary) async {
    final db = await databaseProvider.db();
    summary.id = await db.insert(dao.tableName, dao.toMap(summary));
    return summary;
  }

  Future<Summary> delete(Summary summary) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [summary.id]);
    return summary;
  }

  Future<Summary> update(Summary summary) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(summary),
        where: dao.columnId + " = ?", whereArgs: [summary.id]);
    return summary;
  }

  Future<List<Summary>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<List<Summary>> getDetailsGroupByFipId(int userId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: 'user_id = ?', whereArgs: [userId], groupBy: 'fip_id');
    return dao.fromList(maps);
  }

  Future<Summary> getSummaryByFipId(int userId, String fipId) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.rawQuery(
        'SELECT * FROM summary WHERE user_id=? and fip_id=?', [userId, fipId]);
    return dao.fromMap(maps.first);
  }

  Future<List<Summary>> getSummaryByFipType(int userId, String fipType) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.rawQuery(
        'SELECT * FROM summary WHERE user_id=? and type=?', [userId, fipType]);
    return dao.fromList(maps);
  }
}
