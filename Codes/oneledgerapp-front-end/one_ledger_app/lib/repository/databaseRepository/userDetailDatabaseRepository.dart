import 'dart:async';
import 'package:one_ledger_app/dao/userDetailsDao.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';

class UserDetailDatabaseRepository {
  final dao = UserDetailsDao();

  DatabaseProvider databaseProvider;

  UserDetailDatabaseRepository(this.databaseProvider);

  Future<UserDetails> insert(UserDetails userDetails) async {
    final db = await databaseProvider.db();
    userDetails.id = await db.insert(dao.tableName, dao.toMap(userDetails));
    return userDetails;
  }

  Future<UserDetails> delete(UserDetails userDetails) async {
    final db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + " = ?", whereArgs: [userDetails.id]);
    return userDetails;
  }

  Future<UserDetails> update(UserDetails userDetails) async {
    final db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(userDetails),
        where: dao.columnId + " = ?", whereArgs: [userDetails.id]);
    return userDetails;
  }

  Future<List<UserDetails>> getDetails() async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }

  Future<UserDetails> getDetailByEmail(String email) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: "email = ?", whereArgs: [email], limit: 1);
    if (maps == null) {
      return null;
    }
    return dao.fromMap(maps.first);
  }

  Future<UserDetails> getDetailById(int id) async {
    final db = await databaseProvider.db();
    List<Map> maps = await db.query(dao.tableName,
        where: "id = ?", whereArgs: [id], limit: 1);
    return dao.fromMap(maps.first);
  }
}
