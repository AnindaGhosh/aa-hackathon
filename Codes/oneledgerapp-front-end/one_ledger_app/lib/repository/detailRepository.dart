import 'dart:async';
import 'package:one_ledger_app/utils/databaseProvider.dart';

abstract class DetailRepository {
  DatabaseProvider databaseProvider;

  Future<Object> insert(Object object);

  Future<Object> update(Object object);

  Future<Object> delete(Object object);

  Future<List<Object>> getDetails();
}
