import 'package:one_ledger_app/models/onboarding/personalDetailsModel.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/services/constants.dart';

//functions related to login storage

void makeUserLoggedIn() {
  prefs.setBool("login", true);
}

bool getLoginStatus() {
  return prefs.getBool("login") ?? false;
}

void makeUserLogout() {
  prefs.setBool("login", false);
}

bool getOnboardingStatus() {
  print(prefs.getBool("onboard"));
  return prefs.getBool("onboard") ?? false;
}

void makeUserOnboard() {
  prefs.setBool("onboard", true);
}

void setUserBasicInformation(
    int id, String email, String phoneNumber, String uuid) {
  prefs.setInt("loggedUserId", id);
  prefs.setString("email", email);
  prefs.setString("phoneNumber", phoneNumber);
  prefs.setString("uuid", uuid);
}

Map<String, String> getUserBasicInformation() {
  return {
    "email": prefs.getString("email"),
    "phoneNumber": prefs.getString("phoneNumber"),
    "uuid": prefs.getString("uuid")
  };
}

void setCurrentUserId(int id) {
  prefs.setInt("loggedUserId", id);
}

int getCurrentUserId() {
  return prefs.getInt("loggedUserId");
}

void createUseDetails(PersonalDetailsModel personalDetails) {
  print("Stored successfully");
  prefs.setString("firstName", personalDetails.firstName);
  prefs.setString("lastName", personalDetails.lastName);
  prefs.setInt("dob", personalDetails.dateOfBirth.millisecondsSinceEpoch);
  prefs.setString("gender", personalDetails.gender);
  prefs.setString("pan", personalDetails.panNumber);
  prefs.setString("salarySlab", personalDetails.salarySlab.toString());
}

UserDetails fetchUseDetails() {
  UserDetails userDetails = new UserDetails();
  userDetails.firstName = prefs.getString("firstName");
  userDetails.lastName = prefs.getString("lastName");
  userDetails.email = prefs.getString("email");
  userDetails.phoneNumber = prefs.getString("phoneNumber");
  userDetails.dob = prefs.getInt("dob");
  userDetails.gender = prefs.getString("gender");
  userDetails.pan = prefs.getString("pan");
  userDetails.incomeRange = prefs.getString("salarySlab");
  userDetails.uuid = prefs.getString("uuid");
  return userDetails;
}

void setConsentFetchCompleted() {
  prefs.setBool("allConsentFetched", true);
}

void setConsentFetchIncomplete() {
  prefs.setBool("allConsentFetched", false);
}

bool hasConsentFetchCompleted() {
  return prefs.getBool("allConsentFetched") ?? false;
}

int checkForLoadedConsent() {
  return prefs.getInt("loadConsentCount");
}

void addLoadedConsent() {
  prefs.setInt("loadConsentCount", (prefs.getInt("loadConsentCount") ?? 0) + 1);
}
