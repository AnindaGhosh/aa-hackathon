import 'package:email_validator/email_validator.dart';

String validateEmail(String email) {
  return !EmailValidator.validate(email) ? "Invalid Email" : null;
}

String validatePwd(String pwd) {
  String pattern =
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regExp = new RegExp(pattern);
  return !regExp.hasMatch(pwd) ? "Invalid Password" : null;
}

String validatePhone(String phone) {
  String pattern = r'^(?:[+0]9)?[0-9]{10}$';
  RegExp regExp = new RegExp(pattern);
  return !regExp.hasMatch(phone) ? "Invalid Phone" : null;
}
