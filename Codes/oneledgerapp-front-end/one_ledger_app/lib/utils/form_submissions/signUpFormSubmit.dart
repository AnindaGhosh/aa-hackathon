import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/registerModel.dart';
import 'package:one_ledger_app/screens/otp.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/userManagement.dart';
import 'package:one_ledger_app/widgets/alerts.dart';

signUpCheck({SignUpModal signUpData, BuildContext context}) async {
  if (signUpData.password != signUpData.reEnterPassword)
    showErrorAlert(
        context: context,
        description: "Try Again.",
        title: "Passwords are not same");
  else {
    int response = await signUpAPI(signUpData: signUpData);
    print(response);
    if (response == 1) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Otp(
            email: signUpData.email,
            phoneNumber: signUpData.phoneNumber,
          ),
        ),
      );
    } else {
      handleErrorResponseCodes(code: response, context: context);
    }
  }
}
