import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/repository/databaseRepository/userDetailDatabaseRepository.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

savePersonalDetails(
    {UserDetails personalDetailData, BuildContext context}) async {
  if (personalDetailData != null) {
    DatabaseProvider databaseProvider = DatabaseProvider.get;
    UserDetailDatabaseRepository userDetailDRepo =
        new UserDetailDatabaseRepository(databaseProvider);
    personalDetailData.id = getCurrentUserId();
    (userDetailDRepo.update(personalDetailData))
        .then(
            (savedPersonalDetailData) => {print('Updated user personal data!')})
        .catchError((err) => {
              handleErrorResponseCodes(
                code: responseCodes[1019],
                context: context,
              )
            });
    Navigator.pushNamed(context, '/financialServices');
  } else {
    handleErrorResponseCodes(
      code: 1019,
      context: context,
    );
  }
}
