import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/consentStatus.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/services/aggregatorManagement.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

Future<int> getConsentStatus(
    {ConsentTemplateModel consentTemplateData, BuildContext context}) async {
  ConsentStatus response =
      await getConsentStatusAPI(consentTemplateData: consentTemplateData);
  print(response);
  if (response != null) {
    if (response.active > 0) {
      addLoadedConsent();
      String s = response.active.toString() +
          ' out of ' +
          response.total.toString() +
          ' consents approved!';
      textToLoader.value = s;
      valueToLoader.value += 0.05;
    }
    return 1;
    //Navigator.pushNamed(context, '/loadingPage');
    //!then push to landing page from loading page
  } else {
    handleErrorResponseCodes(
      code: 1019,
      context: context,
    );
  }
  return 0;
}
