import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/financialServicesModel.dart';
import 'package:one_ledger_app/models/sqlit/userFinancialService.dart';
import 'package:one_ledger_app/repository/databaseRepository/userFinancialServicesDatabaseRepository.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

saveFinancialServicesDetails(
    {FinancialServicesModel financialService, BuildContext context}) async {
  if (financialService != null) {
    for (int i = 0; i < financialService.services.length; i++) {
      DatabaseProvider databaseProvider = DatabaseProvider.get;
      UserFinancialServicesDatabaseRepository financialServiceRepo =
          new UserFinancialServicesDatabaseRepository(databaseProvider);

      UserFinancialService userFinancialService = new UserFinancialService();
      userFinancialService.id =
          (new DateTime.now().millisecondsSinceEpoch % 100);
      userFinancialService.userId = getCurrentUserId();
      userFinancialService.serviceName = financialService.services[i];
      (financialServiceRepo.insert(userFinancialService))
          .then((savedUserFinancialService) =>
              print("User financial service saved"))
          .catchError((err) => {
                handleErrorResponseCodes(
                  code: responseCodes[1019],
                  context: context,
                )
              });
    }
    Navigator.pushNamed(context, '/aa');
  } else {
    handleErrorResponseCodes(
      code: 1019,
      context: context,
    );
  }
}
