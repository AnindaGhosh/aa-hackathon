import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/utils/form_submissions/createProfilefromDB.dart';
import 'package:one_ledger_app/utils/form_submissions/consentListFetchFormSubmit.dart';
import 'package:one_ledger_app/utils/form_submissions/consentStatusFormSubmit.dart';
import 'package:one_ledger_app/utils/form_submissions/consentTemplateFormSubmit.dart';
import 'package:one_ledger_app/utils/form_submissions/profileAndSummaryFormSubmit.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

startAutoOnboarding(
    {ConsentTemplateModel consentTemplateData, BuildContext context}) async {
  Navigator.pushNamed(context, '/loadingPage');
  await requestConsent(
      consentTemplateData: consentTemplateData, context: context);
  setConsentFetchIncomplete();
  textToLoader.value =
      'Requested for your consent to the respective AA. Kindly check and approve the same.';
  valueToLoader.value += 0.1;
  while (hasConsentFetchCompleted() != true) {
    await Future.delayed(const Duration(seconds: 10), () {
      getConsentStatus(
          consentTemplateData: consentTemplateData, context: context);
    });
  }
  valueToLoader.value += 0.1;
  List<String> consentIds = await fetchConsentList(
      consentTemplateData: consentTemplateData, context: context);

  await fetchProfileAndSummary(consentList: consentIds, context: context);
  valueToLoader.value += 0.1;
  await createProfileFromDB(context: context);
}
