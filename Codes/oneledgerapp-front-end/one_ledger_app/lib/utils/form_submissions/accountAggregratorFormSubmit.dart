import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/accountAggregatorModel.dart';
import 'package:one_ledger_app/models/sqlit/userRegisteredAccountAggregrator.dart';
import 'package:one_ledger_app/repository/databaseRepository/userAADatabaseRepository.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

saveAADetails({AccountAggregatorModel aaModel, BuildContext context}) async {
  if (aaModel != null) {
    for (int i = 0; i < aaModel.accounts.length; i++) {
      DatabaseProvider databaseProvider = DatabaseProvider.get;
      UserAADatabaseRepository financialServiceRepo =
          new UserAADatabaseRepository(databaseProvider);

      UserRegisteredAccountAggregrator userAA =
          new UserRegisteredAccountAggregrator();
      userAA.id = (new DateTime.now().millisecondsSinceEpoch % 100);
      userAA.userId = getCurrentUserId();
      userAA.aaId = aaModel.accounts[i];
      (financialServiceRepo.insert(userAA))
          .then((savedUserAA) => print("User AA Account saved"))
          .catchError((err) => {
                handleErrorResponseCodes(
                  code: 1019,
                  context: context,
                )
              });
    }
    Navigator.pushNamed(context, '/consentTemplate');
  } else {
    handleErrorResponseCodes(
      code: responseCodes[1019],
      context: context,
    );
  }
}
