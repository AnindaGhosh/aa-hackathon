import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/services/aggregatorManagement.dart';
import 'package:one_ledger_app/services/handleResponse.dart';

requestConsent(
    {ConsentTemplateModel consentTemplateData, BuildContext context}) async {
  int response =
      await requestConsentAPI(consentTemplateData: consentTemplateData);
  print(response);
  if (response == 1) {
    valueToLoader.value += 0.1;
    //!then push to landing page from loading page
  } else {
    handleErrorResponseCodes(
      code: response,
      context: context,
    );
  }
}
