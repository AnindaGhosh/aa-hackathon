import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';
import 'package:one_ledger_app/models/sqlit/account.dart';
import 'package:one_ledger_app/models/sqlit/consentDetails.dart';
import 'package:one_ledger_app/models/sqlit/profile.dart';
import 'package:one_ledger_app/models/sqlit/summary.dart';
import 'package:one_ledger_app/models/sqlit/userRegisteredAccountAggregrator.dart';
import 'package:one_ledger_app/repository/databaseRepository/accountDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/consentDetailDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/profileDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/summaryDatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/userAADatabaseRepository.dart';
import 'package:one_ledger_app/repository/databaseRepository/userDetailDatabaseRepository.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/services/accountManagement.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/utils/databaseProvider.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

createProfileFromDB({BuildContext context}) async {
  DatabaseProvider databaseProvider = DatabaseProvider.get;
  UserDetailDatabaseRepository udr =
      new UserDetailDatabaseRepository(databaseProvider);
  UserDetails userDetails = await udr.getDetailById(getCurrentUserId());
  ProfileDatabaseRepository pfdr =
      new ProfileDatabaseRepository(databaseProvider);
  List<Profile> profileList =
      await pfdr.getDetailsGroupByFipId(getCurrentUserId());
  List<String> fipList = profileList.map((profile) => profile.fipId).toList();
  SummaryDatabaseRepository sdr =
      new SummaryDatabaseRepository(databaseProvider);
  Map<String, dynamic> profileAndSummary = {};
  for (int i = 0; i < fipList.length; i++) {
    Profile p = await pfdr.getSummaryByFipId(getCurrentUserId(), fipList[i]);
    Summary s = await sdr.getSummaryByFipId(getCurrentUserId(), fipList[i]);
    profileAndSummary[fipList[i]] = {
      "profile": jsonEncode(p.toJson()),
      "summary": jsonEncode(s.toJson())
    };
  }

  ConsentDetailDatabaseRepository cddr =
      new ConsentDetailDatabaseRepository(databaseProvider);
  List<ConsentDetail> consentDetails =
      await cddr.getDetailsGroupByConsentId(getCurrentUserId());
  AccountDatabaseRepository adr =
      new AccountDatabaseRepository(databaseProvider);
  for (int i = 0; i < consentDetails.length; i++) {
    List<Account> accList =
        await adr.getAccountListByConsentDetailId(consentDetails[i].id);
    consentDetails[i].accounts = accList;
  }
  UserAADatabaseRepository uaa = new UserAADatabaseRepository(databaseProvider);
  List<UserRegisteredAccountAggregrator> userAAList =
      await uaa.getDetailByUserId(getCurrentUserId());

  List<String> aaList = userAAList.map((userAA) => userAA.aaId).toList();
  List<Map<String, dynamic>> fServices = [];
  List<Map<String, dynamic>> banking = [];
  banking.add(profileAndSummary);
  fServices.add({"Banking": banking, "MutualFunds": []});
  Map<String, dynamic> onBoarding = {
    "uuid": userDetails.uuid,
    "userDetails": (userDetails.toJson()),
    "financialServices": (fServices),
    "aaDetails": (aaList),
    "consentDetails": (consentDetails)
  };

  int response = await callOnboardingAPI(onboarding: onBoarding);
  print(response);
  if (response == 1) {
    textToLoader.value = 'Hurray!! your profile is set..';
    makeUserOnboard();
    valueToLoader.value += (1 - valueToLoader.value);
    //!then push to landing page from loading page
  } else {
    handleErrorResponseCodes(
      code: response,
      context: context,
    );
  }
}
