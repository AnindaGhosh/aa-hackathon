import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/onboarding/consentTemplateModel.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/services/aggregatorManagement.dart';
import 'package:one_ledger_app/services/handleResponse.dart';

Future<List<String>> fetchConsentList(
    {ConsentTemplateModel consentTemplateData, BuildContext context}) async {
  List<String> consentList =
      await getConsentsAPI(consentTemplateData: consentTemplateData);
  //print(consentList);
  if (consentList.length > 0) {
    textToLoader.value = 'Setting up your profile..';
    valueToLoader.value += 0.1;
    return consentList;
    //Navigator.pushNamed(context, '/loadingPage');
    //!then push to landing page from loading page
  } else {
    handleErrorResponseCodes(
      code: 1019,
      context: context,
    );
  }
  return [];
}
