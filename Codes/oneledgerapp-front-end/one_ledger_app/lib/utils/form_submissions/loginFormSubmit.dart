import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/loginModel.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/userManagement.dart';
import 'package:one_ledger_app/utils/sharedPreferences.dart';

loginCheck({LoginModal loginData, BuildContext context}) async {
  int response = await loginAPI(loginData: loginData);
  print(response);
  if (response == 1) {
    makeUserLoggedIn();
    if (getOnboardingStatus())
      Navigator.pushNamed(context, '/landingPage');
    else
      Navigator.pushNamed(context, '/personalDetails');
  } else {
    handleErrorResponseCodes(
      code: response,
      context: context,
    );
  }
}
