import 'package:flutter/material.dart';
import 'package:one_ledger_app/screens/onboarding/loading_indicator_page.dart';
import 'package:one_ledger_app/services/aggregatorManagement.dart';
import 'package:one_ledger_app/services/constants.dart';
import 'package:one_ledger_app/services/handleResponse.dart';

fetchProfileAndSummary({List<String> consentList, BuildContext context}) async {
  int response = await fetchAndSaveProfileAndSummary(consentList: consentList);
  print(response);
  if (response == consentList.length) {
    //valueToLoader.value += 0.1;
    //Navigator.pushNamed(context, '/loadingPage');
    //!then push to landing page from loading page
  } else {
    handleErrorResponseCodes(
      code: 1019,
      context: context,
    );
  }
}
