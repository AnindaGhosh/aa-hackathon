import 'package:flutter/material.dart';
import 'package:one_ledger_app/models/otpModel.dart';
import 'package:one_ledger_app/services/handleResponse.dart';
import 'package:one_ledger_app/services/userManagement.dart';

otpCheck({OtpModal otpData, BuildContext context}) async {
  int response = await otpAPI(otpData: otpData);
  print(response);
  if (response == 1) {
    Navigator.pushNamedAndRemoveUntil(context, '/login', (r) => false);
  } else {
    handleErrorResponseCodes(code: response, context: context);
  }
}
