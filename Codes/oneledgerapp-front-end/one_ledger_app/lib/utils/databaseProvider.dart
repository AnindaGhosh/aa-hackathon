import 'dart:async';
import 'package:one_ledger_app/dao/accountDao.dart';
import 'package:one_ledger_app/dao/consentDetailsDao.dart';
import 'package:one_ledger_app/dao/profileDao.dart';
import 'package:one_ledger_app/dao/summaryDao.dart';
import 'package:one_ledger_app/dao/userDetailsDao.dart';
import 'package:one_ledger_app/dao/userFinancialServicesDao.dart';
import 'package:one_ledger_app/dao/userRegisteredAccountAggregratorDao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseProvider {
  static final _instance = DatabaseProvider._internal();
  static DatabaseProvider get = _instance;
  bool isInitialized = false;
  Database _db;

  DatabaseProvider._internal();

  Future<Database> db() async {
    if (!isInitialized) await _init();
    return _db;
  }

  Future _init() async {
    var databasesPath = await getApplicationDocumentsDirectory();
    String path = join(databasesPath.path, 'olb_app.db');

    _db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(UserDetailsDao().createTableQuery);
      await db.execute(UserFinancialServicesDao().createTableQuery);
      await db.execute(UserRegisteredAccountAggregratorDao().createTableQuery);
      await db.execute(ConsentDetailsDao().createTableQuery);
      await db.execute(AccountDao().createTableQuery);
      await db.execute(ProfileDao().createTableQuery);
      await db.execute(SummaryDao().createTableQuery);
    });
  }
}
