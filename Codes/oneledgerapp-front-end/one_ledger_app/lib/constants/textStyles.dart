import 'package:flutter/cupertino.dart';

var headingTextStyle = TextStyle(
  color: Color(0xff424242),
  fontSize: 18,
  fontFamily: "Source",
);

var onBoardingHeadingTextStyle = TextStyle(
  color: Color(0xff424242),
  fontSize: 18,
  fontFamily: "Source",
  fontWeight: FontWeight.bold,
);

var onBoardingSubHeadingTextStyle = TextStyle(
  color: Color(0xff424242),
  fontSize: 15,
  fontFamily: "Source",
);

var normalTextStyle = TextStyle(
  color: Color(0xff0C1827),
  fontSize: 15,
  fontFamily: "Source",
);

var fieldTextStyle = TextStyle(
  fontFamily: "Source",
  fontSize: 15,
  color: Color(0xff0C1827),
);
var hintTextStyle = TextStyle(
  fontFamily: "Source",
  fontSize: 15,
  color: Color(0xff949494),
);


var linkTextStyle = TextStyle(
  color: Color(0xff39AFC5),
  fontSize: 15,
  fontFamily: "Source",
);

var enabledbuttonTextStyle = TextStyle(
  color: Color(0xffFFFFFF),
  fontSize: 15,
  fontFamily: "Source",
  fontWeight: FontWeight.bold,
);

var disabledbuttonTextStyle = TextStyle(
  color: Color(0xffBDBDBD),
  fontSize: 15,
  fontFamily: "Source",
  fontWeight: FontWeight.bold,
);
var otpTextStyle = TextStyle(
  color: Color(0xff424242),
  fontSize: 25,
  fontFamily: "Source",
  fontWeight: FontWeight.bold,
);
