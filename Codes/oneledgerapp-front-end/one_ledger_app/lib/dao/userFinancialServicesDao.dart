import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/userFinancialService.dart';

class UserFinancialServicesDao implements Dao<UserFinancialService> {
  final tableName = 'financial_services';
  final columnId = 'id';
  final _columnUserId = 'user_id';
  final _columnServiceName = 'service_name';
  final _columnStatus = 'status';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnUserId INTEGER, "
      " $_columnServiceName TEXT, "
      " $_columnStatus TEXT )";

  @override
  UserFinancialService fromMap(Map<String, dynamic> query) {
    UserFinancialService userFinancialService = UserFinancialService();
    userFinancialService.id = query[columnId];
    userFinancialService.userId = query[_columnUserId];
    userFinancialService.serviceName = query[_columnServiceName];
    userFinancialService.status = query[_columnStatus];
    return userFinancialService;
  }

  @override
  Map<String, dynamic> toMap(UserFinancialService object) {
    return <String, dynamic>{
      _columnUserId: object.userId,
      _columnServiceName: object.serviceName,
      _columnStatus: object.status,
    };
  }

  @override
  List<UserFinancialService> fromList(List<Map<String, dynamic>> query) {
    List<UserFinancialService> userFinancialServiceList =
        List<UserFinancialService>();
    for (Map map in query) {
      userFinancialServiceList.add(fromMap(map));
    }
    return userFinancialServiceList;
  }
}
