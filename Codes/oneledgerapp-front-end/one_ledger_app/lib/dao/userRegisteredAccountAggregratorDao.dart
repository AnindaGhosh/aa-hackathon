import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/userRegisteredAccountAggregrator.dart';

class UserRegisteredAccountAggregratorDao
    implements Dao<UserRegisteredAccountAggregrator> {
  final tableName = 'account_aggregrator';
  final columnId = 'id';
  final _columnUserId = 'user_id';
  final _columnAaId = 'aa_id';
  final _columnStatus = 'status';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnUserId INTEGER, "
      " $_columnAaId TEXT, "
      " $_columnStatus TEXT )";

  @override
  UserRegisteredAccountAggregrator fromMap(Map<String, dynamic> query) {
    UserRegisteredAccountAggregrator userRegisteredAccountAggregrator =
        UserRegisteredAccountAggregrator();
    userRegisteredAccountAggregrator.id = query[columnId];
    userRegisteredAccountAggregrator.userId = query[_columnUserId];
    userRegisteredAccountAggregrator.aaId = query[_columnAaId];
    userRegisteredAccountAggregrator.status = query[_columnStatus];
    return userRegisteredAccountAggregrator;
  }

  @override
  Map<String, dynamic> toMap(UserRegisteredAccountAggregrator object) {
    return <String, dynamic>{
      _columnUserId: object.userId,
      _columnAaId: object.aaId,
      _columnStatus: object.status,
    };
  }

  @override
  List<UserRegisteredAccountAggregrator> fromList(
      List<Map<String, dynamic>> query) {
    List<UserRegisteredAccountAggregrator>
        userRegisteredAccountAggregratorList =
        List<UserRegisteredAccountAggregrator>();
    for (Map map in query) {
      userRegisteredAccountAggregratorList.add(fromMap(map));
    }
    return userRegisteredAccountAggregratorList;
  }
}
