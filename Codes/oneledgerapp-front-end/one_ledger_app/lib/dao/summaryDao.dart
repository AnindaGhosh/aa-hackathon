import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/summary.dart';

class SummaryDao implements Dao<Summary> {
  final tableName = 'summary';
  final columnId = 'id';
  final _columnUserId = 'user_id';
  final _columnFipId = 'fip_id';
  final _columnBalanceDateTime = 'balance_date_time';
  final _columnBranch = 'branch';
  final _columnCurrency = 'currency';
  final _columnCurrentBalance = 'current_balance';
  final _columnCurrentODLimit = 'current_od_limit';
  final _columnDrawingLimit = 'drawing_limit';
  final _columnExchgeRate = 'exchge_rate';
  final _columnFacility = 'facility';
  final _columnMicrCode = 'micr_code';
  final _columnOpeningDate = 'opening_date';
  final _columnPendingAmount = 'pending_amount';
  final _columnPendingTransactionType = 'pending_transaction_type';
  final _columnStatus = 'status';
  final _columnType = 'type';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnUserId INTEGER, "
      " $_columnBalanceDateTime DATE, "
      " $_columnFipId TEXT, "
      " $_columnBranch TEXT, "
      " $_columnCurrency TEXT, "
      " $_columnCurrentBalance REAL, "
      " $_columnCurrentODLimit REAL, "
      " $_columnDrawingLimit REAL, "
      " $_columnExchgeRate TEXT, "
      " $_columnFacility TEXT, "
      " $_columnMicrCode TEXT, "
      " $_columnOpeningDate INTEGER , "
      " $_columnPendingAmount INTEGER, "
      " $_columnPendingTransactionType TEXT, "
      " $_columnStatus TEXT, "
      " $_columnType TEXT)";

  @override
  Summary fromMap(Map<String, dynamic> query) {
    Summary profile = Summary();
    profile.id = query[columnId];
    profile.userId = query[_columnUserId];
    profile.fipId = query[_columnFipId];
    profile.balanceDateTime = query[_columnBalanceDateTime];
    profile.branch = query[_columnBranch];
    profile.currency = query[_columnCurrency];
    profile.currentBalance = query[_columnCurrentBalance];
    profile.currentODLimit = query[_columnCurrentODLimit];
    profile.drawingLimit = query[_columnDrawingLimit];
    profile.exchgeRate = query[_columnExchgeRate];
    profile.facility = query[_columnFacility];
    profile.micrCode = query[_columnMicrCode];
    profile.openingDate = query[_columnOpeningDate];
    profile.pendingAmount = query[_columnPendingAmount];
    profile.pendingTransactionType = query[_columnPendingTransactionType];
    profile.status = query[_columnStatus];
    profile.type = query[_columnType];
    return profile;
  }

  @override
  Map<String, dynamic> toMap(Summary object) {
    return <String, dynamic>{
      _columnUserId: object.userId,
      _columnBalanceDateTime: object.balanceDateTime,
      _columnFipId: object.fipId,
      _columnBranch: object.branch,
      _columnCurrency: object.currency,
      _columnCurrentBalance: object.currentBalance,
      _columnCurrentODLimit: object.currentODLimit,
      _columnDrawingLimit: object.drawingLimit,
      _columnExchgeRate: object.exchgeRate,
      _columnFacility: object.facility,
      _columnMicrCode: object.micrCode,
      _columnOpeningDate: object.openingDate,
      _columnPendingAmount: object.pendingAmount,
      _columnPendingTransactionType: object.pendingTransactionType,
      _columnStatus: object.status,
      _columnType: object.type
    };
  }

  @override
  List<Summary> fromList(List<Map<String, dynamic>> query) {
    List<Summary> profileList = List<Summary>();
    for (Map map in query) {
      profileList.add(fromMap(map));
    }
    return profileList;
  }
}
