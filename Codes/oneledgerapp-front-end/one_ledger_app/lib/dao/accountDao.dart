import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/account.dart';

class AccountDao implements Dao<Account> {
  final tableName = 'account';
  final columnId = 'id';
  final _columnConsentDetailId = 'consent_detail_id';
  final _columnFipName = 'fip_name';
  final _columnFipId = 'fip_id';
  final _columnAccountType = 'account_type';
  final _columnLinkReferenceNumber = 'link_reference_number';
  final _columnMaskedAccountNumber = 'masked_account_number';
  final _columnFiType = 'fi_type';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnConsentDetailId INTEGER, "
      " $_columnFipName TEXT, "
      " $_columnFipId TEXT, "
      " $_columnAccountType TEXT, "
      " $_columnLinkReferenceNumber TEXT, "
      " $_columnMaskedAccountNumber TEXT, "
      " $_columnFiType TEXT) ";

  @override
  Account fromMap(Map<String, dynamic> query) {
    Account account = Account();
    account.id = query[columnId];
    account.consentDetailId = query[_columnConsentDetailId];
    account.fipName = query[_columnFipName];
    account.fipId = query[_columnFipId];
    account.accountType = query[_columnAccountType];
    account.linkReferenceNumber = query[_columnLinkReferenceNumber];
    account.maskedAccountNumber = query[_columnMaskedAccountNumber];
    account.fiType = query[_columnFiType];
    return account;
  }

  @override
  Map<String, dynamic> toMap(Account object) {
    return <String, dynamic>{
      _columnConsentDetailId: object.consentDetailId,
      _columnFipName: object.fipName,
      _columnFipId: object.fipId,
      _columnAccountType: object.accountType,
      _columnLinkReferenceNumber: object.linkReferenceNumber,
      _columnMaskedAccountNumber: object.maskedAccountNumber,
      _columnFiType: object.fiType
    };
  }

  @override
  List<Account> fromList(List<Map<String, dynamic>> query) {
    List<Account> userDetailList = List<Account>();
    for (Map map in query) {
      userDetailList.add(fromMap(map));
    }
    return userDetailList;
  }
}
