import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/profile.dart';

class ProfileDao implements Dao<Profile> {
  final tableName = 'profile';
  final columnId = 'id';
  final _columnUserId = 'user_id';
  final _columnFipId = 'fip_id';
  final _columnAddress = 'address';
  final _columnCkycCompliance = 'ckyc_compliance';
  final _columnDob = 'dob';
  final _columnEmail = 'email';
  final _columnLandline = 'landline';
  final _columnMobile = 'mobile';
  final _columnName = 'name';
  final _columnNominee = 'nominee';
  final _columnPan = 'pan';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnUserId INTEGER, "
      " $_columnFipId INTEGER, "
      " $_columnAddress TEXT, "
      " $_columnCkycCompliance TEXT, "
      " $_columnDob INTEGER , "
      " $_columnEmail TEXT, "
      " $_columnLandline TEXT, "
      " $_columnMobile TEXT, "
      " $_columnName TEXT, "
      " $_columnNominee TEXT, "
      " $_columnPan TEXT)";

  @override
  Profile fromMap(Map<String, dynamic> query) {
    Profile profile = Profile();
    profile.id = query[columnId];
    profile.userId = query[_columnUserId];
    profile.fipId = query[_columnFipId];
    profile.address = query[_columnAddress];
    profile.ckycCompliance = query[_columnCkycCompliance];
    profile.dob = query[_columnDob];
    profile.email = query[_columnEmail];
    profile.landline = query[_columnLandline];
    profile.mobile = query[_columnMobile];
    profile.name = query[_columnName];
    profile.nominee = query[_columnNominee];
    profile.pan = query[_columnPan];
    return profile;
  }

  @override
  Map<String, dynamic> toMap(Profile object) {
    return <String, dynamic>{
      _columnUserId: object.userId,
      _columnFipId: object.fipId,
      _columnAddress: object.address,
      _columnCkycCompliance: object.ckycCompliance,
      _columnDob: object.dob,
      _columnEmail: object.email,
      _columnLandline: object.landline,
      _columnMobile: object.mobile,
      _columnName: object.name,
      _columnNominee: object.nominee,
      _columnPan: object.pan,
    };
  }

  @override
  List<Profile> fromList(List<Map<String, dynamic>> query) {
    List<Profile> profileList = List<Profile>();
    for (Map map in query) {
      profileList.add(fromMap(map));
    }
    return profileList;
  }
}
