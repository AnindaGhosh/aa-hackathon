import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/consentDetails.dart';

class ConsentDetailsDao implements Dao<ConsentDetail> {
  final tableName = 'consent_detail';
  final columnId = 'id';
  final _columnUserId = 'user_id';
  final _columnConsentId = 'consent_id';
  final _columnConsentHandle = 'consent_handle';
  final _columnProductId = 'product_id';
  final _columnAccountId = 'account_id';
  final _columnAaId = 'aa_id';
  final _columnVua = 'vua';
  final _columnConsentCreationDate = 'consent_creation_date';
  final _columnStatus = 'status';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnUserId INTEGER, "
      " $_columnConsentId TEXT, "
      " $_columnConsentHandle TEXT, "
      " $_columnProductId TEXT, "
      " $_columnAccountId TEXT, "
      " $_columnAaId TEXT, "
      " $_columnVua TEXT, "
      " $_columnConsentCreationDate INTEGER, "
      " $_columnStatus TEXT )";

  @override
  ConsentDetail fromMap(Map<String, dynamic> query) {
    ConsentDetail consentDetail = ConsentDetail();
    consentDetail.id = query[columnId];
    consentDetail.userId = query[_columnUserId];
    consentDetail.consentId = query[_columnConsentId];
    consentDetail.consentHandle = query[_columnConsentHandle];
    consentDetail.productId = query[_columnProductId];
    consentDetail.accountId = query[_columnAccountId];
    consentDetail.aaId = query[_columnAaId];
    consentDetail.vua = query[_columnVua];
    consentDetail.consentCreationDate = query[_columnConsentCreationDate];
    consentDetail.status = query[_columnStatus];
    return consentDetail;
  }

  @override
  Map<String, dynamic> toMap(ConsentDetail object) {
    return <String, dynamic>{
      _columnUserId: object.userId,
      _columnConsentId: object.consentId,
      _columnConsentHandle: object.consentHandle,
      _columnProductId: object.productId,
      _columnAccountId: object.accountId,
      _columnAaId: object.aaId,
      _columnVua: object.vua,
      _columnConsentCreationDate: object.consentCreationDate,
      _columnStatus: object.status,
    };
  }

  @override
  List<ConsentDetail> fromList(List<Map<String, dynamic>> query) {
    List<ConsentDetail> consentDetailList = List<ConsentDetail>();
    for (Map map in query) {
      consentDetailList.add(fromMap(map));
    }
    return consentDetailList;
  }
}
