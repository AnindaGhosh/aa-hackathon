import 'package:one_ledger_app/dao/dao.dart';
import 'package:one_ledger_app/models/sqlit/UserDetails.dart';

class UserDetailsDao implements Dao<UserDetails> {
  final tableName = 'user_details';
  final columnId = 'id';
  final _columnFirstName = 'first_name';
  final _columnLastName = 'last_name';
  final _columnEmail = 'email';
  final _columnPhoneNumber = 'phone_number';
  final _columnDob = 'dob';
  final _columnGender = 'gender';
  final _columnPan = 'pan';
  final _columnIncomeRange = 'income_range';
  final _columnUuid = 'uuid';

  @override
  String get createTableQuery =>
      "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,"
      " $_columnFirstName TEXT, "
      " $_columnLastName TEXT, "
      " $_columnEmail TEXT, "
      " $_columnPhoneNumber TEXT, "
      " $_columnDob INTEGER , "
      " $_columnGender TEXT, "
      " $_columnPan TEXT, "
      " $_columnIncomeRange TEXT, "
      " $_columnUuid TEXT) ";

  @override
  UserDetails fromMap(Map<String, dynamic> query) {
    UserDetails userDetails = UserDetails();
    userDetails.id = query[columnId];
    userDetails.firstName = query[_columnFirstName];
    userDetails.lastName = query[_columnLastName];
    userDetails.email = query[_columnEmail];
    userDetails.phoneNumber = query[_columnPhoneNumber];
    userDetails.dob = query[_columnDob];
    userDetails.gender = query[_columnGender];
    userDetails.pan = query[_columnPan];
    userDetails.incomeRange = query[_columnIncomeRange];
    userDetails.uuid = query[_columnUuid];
    return userDetails;
  }

  @override
  Map<String, dynamic> toMap(UserDetails object) {
    return <String, dynamic>{
      _columnFirstName: object.firstName,
      _columnLastName: object.lastName,
      _columnEmail: object.email,
      _columnPhoneNumber: object.phoneNumber,
      _columnDob: object.dob,
      _columnGender: object.gender,
      _columnPan: object.pan,
      _columnIncomeRange: object.incomeRange,
      _columnUuid: object.uuid
    };
  }

  @override
  List<UserDetails> fromList(List<Map<String, dynamic>> query) {
    List<UserDetails> userDetailList = List<UserDetails>();
    for (Map map in query) {
      userDetailList.add(fromMap(map));
    }
    return userDetailList;
  }
}
